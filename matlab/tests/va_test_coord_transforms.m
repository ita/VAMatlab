function va_test_coord_transforms()

OPENGL_DEFAULT_VIEW = [0 0 -1];
OPENGL_DEFAULT_BACK = [0 0 1];
OPENGL_DEFAULT_UP = [0 1 0];
OPENGL_DEFAULT_BOTTOM = [0 -1 0];
OPENGL_DEFAULT_LEFT = [-1 0 0];
OPENGL_DEFAULT_RIGHT = [1 0 0];

MAT_DEFAULT_VIEW = [1 0 0];
MAT_DEFAULT_BACK = [-1 0 0];
MAT_DEFAULT_UP = [0 0 1];
MAT_DEFAULT_BOTTOM = [0 0 -1];
MAT_DEFAULT_LEFT = [0 1 0];
MAT_DEFAULT_RIGHT = [0 -1 0];

UNREAL_DEFAULT_VIEW = [1 0 0];
UNREAL_DEFAULT_BACK = [-1 0 0];
UNREAL_DEFAULT_UP = [0 0 1];
UNREAL_DEFAULT_BOTTOM = [0 0 -1];
UNREAL_DEFAULT_LEFT = [0 -1 0];
UNREAL_DEFAULT_RIGHT = [0 1 0];

disp('---VA COORD TRAFO TEST---')
%% Matlab to OpenGL
test_single_transform(@va_matlab2openGL,...
    MAT_DEFAULT_VIEW, MAT_DEFAULT_BACK, MAT_DEFAULT_UP, MAT_DEFAULT_BOTTOM, MAT_DEFAULT_LEFT, MAT_DEFAULT_RIGHT,...
    OPENGL_DEFAULT_VIEW, OPENGL_DEFAULT_BACK, OPENGL_DEFAULT_UP, OPENGL_DEFAULT_BOTTOM, OPENGL_DEFAULT_LEFT, OPENGL_DEFAULT_RIGHT);


%% OpenGL to Matlab
test_single_transform(@va_openGL2matlab,...
    OPENGL_DEFAULT_VIEW, OPENGL_DEFAULT_BACK, OPENGL_DEFAULT_UP, OPENGL_DEFAULT_BOTTOM, OPENGL_DEFAULT_LEFT, OPENGL_DEFAULT_RIGHT,...
    MAT_DEFAULT_VIEW, MAT_DEFAULT_BACK, MAT_DEFAULT_UP, MAT_DEFAULT_BOTTOM, MAT_DEFAULT_LEFT, MAT_DEFAULT_RIGHT);

%% Unreal to Matlab
test_single_transform(@va_unreal2matlab,...
    UNREAL_DEFAULT_VIEW, UNREAL_DEFAULT_BACK, UNREAL_DEFAULT_UP, UNREAL_DEFAULT_BOTTOM, UNREAL_DEFAULT_LEFT, UNREAL_DEFAULT_RIGHT,...
    MAT_DEFAULT_VIEW, MAT_DEFAULT_BACK, MAT_DEFAULT_UP, MAT_DEFAULT_BOTTOM, MAT_DEFAULT_LEFT, MAT_DEFAULT_RIGHT);

%% Unreal to OpenGL
test_single_transform(@va_unreal2openGL,...
    UNREAL_DEFAULT_VIEW, UNREAL_DEFAULT_BACK, UNREAL_DEFAULT_UP, UNREAL_DEFAULT_BOTTOM, UNREAL_DEFAULT_LEFT, UNREAL_DEFAULT_RIGHT,...
    OPENGL_DEFAULT_VIEW, OPENGL_DEFAULT_BACK, OPENGL_DEFAULT_UP, OPENGL_DEFAULT_BOTTOM, OPENGL_DEFAULT_LEFT, OPENGL_DEFAULT_RIGHT);

disp('-------------------------')

function test_single_transform(trafo_func_handle,...
    trafo_view, trafo_back, trafo_up, trafo_bottom, trafo_left, trafo_right,...
    comp_view, comp_back, comp_up, comp_bottom, comp_left, comp_right)

if contains(func2str(trafo_func_handle), 'unreal')
    scale_factor = 100;
else
    scale_factor = 1;
end

disp(['Testing "' func2str(trafo_func_handle) '"function'])
ok = true;
if ~isequal( trafo_func_handle(trafo_view)*scale_factor, comp_view )
    disp(['  -> ' func2str(trafo_func_handle) ': Error transforming view vector.' ])
    ok = false;
end
if ~isequal( trafo_func_handle(trafo_back)*scale_factor, comp_back )
    disp(['  -> ' func2str(trafo_func_handle) ': Error transforming back vector.' ])
    ok = false;
end
if ~isequal( trafo_func_handle(trafo_up)*scale_factor, comp_up )
    disp(['  -> ' func2str(trafo_func_handle) ': Error transforming up vector.' ])
    ok = false;
end
if ~isequal( trafo_func_handle(trafo_bottom)*scale_factor, comp_bottom )
    disp(['  -> ' func2str(trafo_func_handle) ': Error transforming bottom vector.' ])
    ok = false;
end
if ~isequal( trafo_func_handle(trafo_left)*scale_factor, comp_left )
    disp(['  -> ' func2str(trafo_func_handle) ': Error transforming left vector.' ])
    ok = false;
end
if ~isequal( trafo_func_handle(trafo_right)*scale_factor, comp_right )
    disp(['  -> ' func2str(trafo_func_handle) ': Error transforming right vector.' ])
    ok = false;
end

if ok
    disp('PASSED')
else
    disp('FAILED')
end