%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% VA EXAMPLE: CAR PASS-BY %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% In this example a real-time auralization of a pass-by of an electrical car is carried out. For this purpose,
% the street is assumed as flat surface meaning we consider the direct sound and a ground reflection.
% This is achieved using the AirtrafficNoiseRenderer with homogeneous medium and disabling turbulence
% (see https://www.virtualacoustics.org/VA/documentation/rendering/#airtrafficnoise).
% Generally, a car consists of a set of partial sound sources. Here, three sources are considered:
% - roll noise of front wheels (front axis)
% - roll noise of rear wheels (rear axis)
% - Acoustic Vehicle Alert System (AVAS)

% You will learn how to:
%   - Setup the AirTrafficNoise renderer for a car pass-by
%   - Use partial sound sources
%   - Specify a reflection factor for the ground
%   - Run such an auralization using the convenience VAScenario class

% IMPORTANT: When running dynamic scenarios like this, it is highly
% recommended to use an ASIO driver (instead of Portaudio). For this
% purpose, you have to manually change the Audio Device in the ini-file.
% For more information, see www.virtualacoustics.org/VA/documentation/configuration/#audio-driver-configuration.
%
% Also, make sure to run VAMatlab_setup.m before running this example.

%% Related examples
% Before running this tutorial, it is recommended to learn about VAMatlab's
% convenience classes for the auralization of dynamic scenes:
% - VA_example_dynamic_scene_convenience_classes.m
%
% For (real-time) auralization of aircraft noise check:
% - VA_example_aircraft_offline.m
% - VA_example_aircraft_realtime.m

%% Some words to partial sound sources
% Generally, VA does not have special objects for partial sound sources. In
% other words, they are just a series of sound sources each having its own
% trajectory, signal source and directivity. However, some of this data is
% usually similar data or even identical.

% For example, the trajectories are the quasi-identical the same but simply
% shifted depending on the position of the partial sound source within the
% car. Thus, it makes sense to first define the trajectory for the car in
% general and then simply shift it for each partial sound source.

% In case of the roll noise of front and back wheels, also the underlying
% source model is identical. Thus, it is valid to use the same directivity.
% However, note that the signal must not identical but just have the same
% properties (e.g. noise shape). Using the same signals would mean that
% they are fully correlated leading to implausible, metallic-sounding
% phasing effects.
% Note that generally, the road-tyre noise component level (controlled via
% the source sound power) depends on the actual speed and acceleration
% state of the car. In this example, this is simplified by using a constant
% sound power referenced to 50 km/h. However, the respective spectrum does
% not depend on speed or acceleration state.

%% Configuration parameters (VACore.ini)
va_ini_file = 'VACore.Car.ini';
renderer_id = 'MyCarPassByRenderer';
sampling_rate = 44100;

%% SCENARIO INPUT DATA %%
%%%%%%%%%%%%%%%%%%%%%%%%%
%% Receiver
receiver_position = [0 1.7 0];              % Center of coordinate system, height of 1.7m
receiver_view = [0 0 -1];                   % View: negative z (to the front)
receiver_up = [0 1 0];                      % Up: positive y

receiver_directivity = '$(DefaultHRIR)';    % Receiver directivity file (see VACore.Car.ini [Macros] section)

%% Car trajectory (center of car)
car_start_position = [-40 0 -25];           % From receiver: 40m to the left and 25m away. Actual altitude (y-value) will be specified later for each partial source, respectively.
car_view = [1 0 0];                         % View: in x direction, the direction of the trajectory
car_up = [0 1 0];                           % Up: positive y
car_start_velocity = 10 / 3.6;              % Velocity: 10km/h -> ~2.8m/s
car_end_velocity = 50 / 3.6;                % Velocity: 50km/h -> ~14m/s
car_start_velocity_duration = 3;            % First segment of trajectory: constant velocity
car_acceleration_duration = 5;              % Second segment: car accelerates
car_end_velocity_duration = 2;              % Third segment: constant velocity

%% Partial sound sources data
% The respective files are located within VA's 'data' folder. Check the
% LICENSE.md for information on license and literature references to
% underlying models.
tyre_noise_front_signal = 'TyreNoise_Front.wav';
tyre_noise_rear_signal = 'TyreNoise_Rear.wav';
avas_signal = 'Electric_AVAS_3_5_5sec.wav';

avas_directivity = '$(TrumpetDir)';         % Assuming the avas sound is emitted by a horn loudspeaker with an opening similar to a trumpet.
tyre_noise_directivity = 'TyreNoise.daff';

sound_power_tyre_noise = 0.02;  %[W]
sound_power_avas = 0.8;         %[W]

%% Real-time rendering variables
motion_update_rate = 60;                    % How many times a second the positions are updated

%% One-third octave band reflection factor (porous asphalt)
% Check the following publication for more info on this data:
% Dreier, C., Vorländer, M. Vehicle pass-by noise auralization in a virtual urban environment. In: Proc. INTER-NOISE, Glasgow, Scotland, 2022
reflection_factor = [0.99382 0.99228 0.99028 0.98767 0.98461 0.980794 0.97546 0.96938 0.96181 0.95271 0.93905 0.92377 0.90356 0.87615 0.84145 0.78960 0.70004 0.52444 0.20321 0.63507 0.83867 0.95117 0.87258 0.39983 0.91094 0.67610 0.95489 0.79735 0.70340 0.94585 0.549254];

%% PREPARING OBJECTS FOR AURALIZATION %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Static receiver
receiver = VAReceiver( VATrajectory(0, receiver_position, receiver_view, receiver_up), receiver_directivity );

%% General car trajectory
t1 = 0;
t2 = t1 + car_start_velocity_duration;
t3 = t2 + car_acceleration_duration;
t4 = t3 + car_end_velocity_duration;

trajectory_part1 = VATrajectory.CreateLinear(car_start_position, car_view, car_up, car_start_velocity, t2, t1);

car_acceleration = (car_end_velocity -car_start_velocity) / car_acceleration_duration;
acceleration_start_position = trajectory_part1.position(end,:);
trajectory_part2 = VATrajectory.CreateLinearUniformlyAccelerated(acceleration_start_position, car_view ,car_up, car_start_velocity, car_acceleration, t3, motion_update_rate, t2);

trajectory_part3 = VATrajectory.CreateLinear(trajectory_part2.position(end,:), car_view, car_up, car_end_velocity, t4, t3);

car_center_trajectory = trajectory_part1 + trajectory_part2 + trajectory_part3;
raw_car_positions = car_center_trajectory.position;

%% Trajectories of partial sound sources
% Adjust positions for individual sources
raw_car_positions(:, 2) = 0.10; % Wheel altitude above street
position_wheel_front = raw_car_positions + car_view * 1.25;
position_wheel_rear = raw_car_positions - car_view * 1.25;

position_avas_horn = raw_car_positions + car_view * 1.75; % At front of the car
position_avas_horn(:, 2) = 0.30; % Altitude of horn used for AVAS sounds above street

% Final trajectory objects
trajectory_wheel_front = VATrajectory(car_center_trajectory.time, position_wheel_front, car_view, car_up);
trajectory_wheel_rear = VATrajectory(car_center_trajectory.time, position_wheel_rear, car_view, car_up);
trajectory_avas_horn = VATrajectory(car_center_trajectory.time, position_avas_horn, car_view, car_up);

%% Partial sound sources
tyre_noise_front = VASource(trajectory_wheel_front, tyre_noise_front_signal, sound_power_tyre_noise, tyre_noise_directivity, 'car_tyre_noise_front');
tyre_noise_rear = VASource(trajectory_wheel_rear, tyre_noise_rear_signal, sound_power_tyre_noise, tyre_noise_directivity, 'car_tyre_noise_rear');
car_avas = VASource(trajectory_avas_horn, avas_signal, sound_power_avas, avas_directivity, 'car_avas');
sound_sources = [tyre_noise_front, tyre_noise_rear, car_avas];


%% Scenario class
scenario = VAScenario;
scenario.sampling_rate = 44100;
scenario.motion_update_rate = motion_update_rate;
scenario.va_ini_path = which(va_ini_file);
scenario.renderer_id = renderer_id;
scenario.source_receiver_init_parameters.reflected_path.ground_reflection_third_octaves = reflection_factor;
scenario.global_auralization_mode = '-TV'; % Disabling turbulence

%% RUN AURALIZATION %%
%%%%%%%%%%%%%%%%%%%%%%
scenario.realtime_rendering(sound_sources, receiver, t4);
