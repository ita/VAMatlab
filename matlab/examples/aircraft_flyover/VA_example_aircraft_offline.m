%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% VA EXAMPLE: AIRCRAFT FLYOVER - OFFLINE %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% In this example, multiple offline auralizations of an aircraft flyover are carried out.
% using the AirTrafficNoise renderer (www.virtualacoustics.org/VA/documentation/rendering/#airtrafficnoise)
% which considers the direct sound and the reflection at a flat ground. For all scenarios,
% the same aircraft trajectory (aircraft flying from left to right at 1km altitude) is
% used, while varying the atmospheric conditions:
% 1. homogeneous medium
% 2. inhomogeneous, moving medium - weather-data based on analytic models
% 3. inhomogeneous, moving medium - measured weather-data (summer)
% 4. inhomogeneous, moving medium - measured weather-data (autumn)

% When using the inhomogeneous, moving medium VA carries out simulations using the
% Atmospheric Ray Tracing (ART) framework (www.virtualacoustics.org/GA/art/) which
% simulates curved sound propagation paths. It is important to know, that ART uses the
% right-handed mathematical coordinate system instead of the OpenGL convention that VA
% uses. This is mainly relevant, when setting up the wind direction. Thus, it is a good
% approach to define the scenario in mathematical coordinates and uses utils functions
% provided by VAMatlab to transform to OpenGL.

% You will learn how to:
%   - Setup the AirTrafficNoise renderer either with a homogeneous or inhomogeneous medium
%   - Specify the data for a homogeneous / inhomogeneous medium
%   - Transform between coordinates systems
%   - Run such an auralization using the convenience classes

% IMPORTANT: Make sure to run VAMatlab_setup.m before running this example

%% Related examples
% Before running this tutorial, it is recommended to learn about VAMatlab's
% convenience classes for the auralization of dynamic scenes:
% - VA_example_dynamic_scene_convenience_classes.m
%
% For real-time auralization of aircraft noise check:
% - VA_example_aircraft_realtime.m

%% Configuration parameters (VACore.ini)
va_ini_homogeneous = 'VACore.AircraftHomogeneous.offline.ini';
va_ini_inhomogeneous = 'VACore.AircraftInhomogeneous.offline.ini';
renderer_id = 'MyAirTrafficNoiseRenderer';
sampling_rate = 44100;  % Make sure it matches the sampling rate in the .ini file
block_size = 1024;      % Make sure it matches the buffer size in the .ini file

%% Scene Parameters
% Definition in mathematical (Matlab) coordinates, which are then
% transformed to OpenGL. The ground is the xy-plane (z = 0).

receiver_position = va_matlab2openGL( [0 0 1.7] ); % Center of coordinate system, height of 1.7m
receiver_view = va_matlab2openGL( [0 1 0] );       % View: positive y
receiver_up = va_matlab2openGL( [0 0 1] );         % Up: positive z

receiver_directivity = '$(DefaultHRIR)';

% Source
source_start_position = va_matlab2openGL([-1000 500 1000]); % From receiver: 1000m to the left, 500m to the front, at 1000m altitude
source_view = va_matlab2openGL([1 0 0]);                    % View: in x direction, the direction of the trajectory
source_up = va_matlab2openGL([0 0 1]);                      % Up: positive z
source_velocity = 84;                                       % Velocity: 84m/s -> 302.4km/h

source_directivity = '';                    % Default empty string for no specific directivity.

signal_source_file = '$(AircraftNoise)';    % Signal source file (see [Macros] section in .ini file)

source_SPL = 130;                           % Source sound power Level [dB]
source_SP = 10^(source_SPL/10)*1e-12;       % Sound power level to sound power [W]

% CAUTION : Changing source position or source SPL may result in high
% output levels. Be careful with you hearing when experimenting with these
% values.

% Scenario
time_start = 0;                             % Time when simulation starts
time_end = 20;                              % Time when trajectory ends

examples_folder = fileparts( which(va_ini_inhomogeneous) );
rendering_output_folder = fullfile( examples_folder, 'recordings' );
rendering_output_file_homogeneous = 'aircraft_flyover_homogeneous.wav';
rendering_output_file_inhomogeneous_analytic = 'aircraft_flyover_inhomogeneous_analytic.wav';
rendering_output_file_inhomogeneous_summer = 'aircraft_flyover_inhomogeneous_summer.wav';
rendering_output_file_inhomogeneous_autumn = 'aircraft_flyover_inhomogeneous_autumn.wav';

%% Homogeneous medium data
% Homogeneous medium is specified in 'VACore.AircraftHomogeneous.recording.ini'
% in [HomogeneousMedium] section. Note, that it can also be specified
% during run-time:
% https://www.virtualacoustics.org/VA/documentation/scene/#homogeneous-medium

%% Inhomogeneous medium data
% The following data files were created using the StratifiedAtmosphere
% class of the ART Matlab interface. See 'ARTTutorial_StratifiedAtmosphere.m'
% tutorial, for information on how to define and store such data.
stratified_atmosphere_file_summer = which( 'atmosphere_summer.json' );
stratified_atmosphere_file_autumn = which( 'atmosphere_autumn.json' );

% If not specifying the atmosphere, ART uses analytic models for temperature,
% static pressure and wind velocity. Those are the International Standard
% Atmosphere (ISA) and the logarithmic wind profile. The wind direction and
% relative humidity are assumed to be constant ([1 0 0] and 50%).


%% Source and receiver trajectories
source_trajectory = VATrajectory.CreateLinear( source_start_position, source_view, ...
    source_up, source_velocity, time_end, time_start );

receiver_trajectory = VATrajectory( [time_start ; time_end], ...
    receiver_position, receiver_view, receiver_up );

%% Source and receiver objects
receiver = VAReceiver( receiver_trajectory , receiver_directivity , 'VA_listener' );
source = VASource( source_trajectory, signal_source_file, source_SP, source_directivity , 'VA_source' );

%% Scenario for offline auralization

scenario = VAScenario();

scenario.sampling_rate = sampling_rate;
scenario.block_size = block_size;
scenario.renderer_id = renderer_id;
scenario.rendering_output_folder = rendering_output_folder;

% This setting adds some "idle" processing of blocks in the beginning to
% fill the VDL buffer. The respective time is cropped from the resulting
% recordings. Otherwise, there would be no sound in the beginning due to
% the initial propagation delay.
scenario.preprocessing_time = 10;

%% AURALIZATIONS %%
%%%%%%%%%%%%%%%%%%%
%% Straight paths / Homogeneous medium
scenario.rendering_output_file = rendering_output_file_homogeneous;
scenario.va_ini_path = which(va_ini_homogeneous);
scenario.renderering_init_parameters = struct();
scenario.offline_rendering( source, receiver, time_end);

%% Curved sound paths / Inhomogeneous moving medium (default / analytic)
scenario.rendering_output_file = rendering_output_file_inhomogeneous_analytic;
scenario.va_ini_path = which(va_ini_inhomogeneous);
scenario.renderering_init_parameters = struct();
scenario.offline_rendering( source, receiver, time_end);

%% Curved sound paths / Inhomogeneous moving medium (summer)
scenario.rendering_output_file = rendering_output_file_inhomogeneous_summer;
scenario.va_ini_path = which(va_ini_inhomogeneous);
scenario.renderering_init_parameters.stratified_atmosphere = stratified_atmosphere_file_summer;
scenario.offline_rendering( source, receiver, time_end);

%% Curved sound paths / Inhomogeneous moving medium (autum)
scenario.rendering_output_file = rendering_output_file_inhomogeneous_autumn;
scenario.va_ini_path = which(va_ini_inhomogeneous);
scenario.renderering_init_parameters.stratified_atmosphere = stratified_atmosphere_file_autumn;
scenario.offline_rendering( source, receiver, time_end);

%% PLAYBACK RESULTS %%
%%%%%%%%%%%%%%%%%%%%%%
%% Homogeneous medium
[rendered_audio, Fs] = audioread( fullfile( rendering_output_folder, rendering_output_file_homogeneous ) );
player = audioplayer( rendered_audio, Fs );
play( player );

%% Inhomogeneous medium / default
[rendered_audio, Fs] = audioread( fullfile( rendering_output_folder, rendering_output_file_inhomogeneous_analytic ) );
player = audioplayer( rendered_audio, Fs );
play( player );

%% Inhomogeneous medium / summer
[rendered_audio, Fs] = audioread( fullfile( rendering_output_folder, rendering_output_file_inhomogeneous_summer ) );
player = audioplayer( rendered_audio, Fs );
play( player );

%% Inhomogeneous medium / autumn
[rendered_audio, Fs] = audioread( fullfile( rendering_output_folder, rendering_output_file_inhomogeneous_autumn ) );
player = audioplayer( rendered_audio, Fs );
play( player );

%% PLOTTING RESULTS %%
%%%%%%%%%%%%%%%%%%%%%%
if ~exist('itaAudio', 'class')
    error('This code block requires installing the ITA-Toolbox. Visit "www.ita-toolbox.org" for more information and download.');
end

audio_homogeneous = ita_read( fullfile( rendering_output_folder, rendering_output_file_homogeneous ) );
audio_inhomogeneous_analytic = ita_read( fullfile( rendering_output_folder, rendering_output_file_inhomogeneous_analytic ) );
audio_summer = ita_read( fullfile( rendering_output_folder, rendering_output_file_inhomogeneous_summer ) );
audio_autumn = ita_read( fullfile( rendering_output_folder, rendering_output_file_inhomogeneous_autumn ) );

merged_left_channels = ita_merge(audio_homogeneous.ch(1), audio_inhomogeneous_analytic.ch(1), audio_summer.ch(1), audio_autumn.ch(1));
merged_left_channels.channelNames = {'Homogeneous', 'Inhom. Analytic', 'Inhom. Summer', 'Inhom. Autumn'};
merged_left_channels.plot_freq;

ax = gca;
ax.Children = ax.Children([1 2 4 5 6 3]);
ax.Title.String = 'Aircraft flyover spectra';
ax.Legend.Location = 'NorthEast';