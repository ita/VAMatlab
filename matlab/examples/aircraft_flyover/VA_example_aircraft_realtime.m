%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% VA EXAMPLE: AIRCRAFT FLYOVER - REAL-TIME %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% In this example, a real-time auralization of an aircraft flyover is carried out using the AirtrafficNoise renderer
% (www.virtualacoustics.org/VA/documentation/rendering/#airtrafficnoise) which assumes a flat ground. 
% In this scenario, the receiver is static receiver and the aircraft flies with constant velocity from left to right at one kilometer altitude.

% In this example, you will learn how to:
%   - Setup the AirTrafficNoise renderer for real-time considering
%       - straight sound paths (homogeneous medium)
%       - curved sound paths (inhomogeneous medium)
%   - Using a timer to transfer the aircraft motion data to VA

% IMPORTANT: When running dynamic scenarios like this, it is highly
% recommended to use an ASIO driver (instead of Portaudio). For this
% purpose, you have to manually change the Audio Device in the ini-file.
% For more information, see www.virtualacoustics.org/VA/documentation/configuration/#audio-driver-configuration.
%
% Also, make sure to run VAMatlab_setup.m before running this example.

%% Related examples
% Before running this tutorial, it is recommended to run the following examples:
% - VA_example_aircraft_offline

%% Homogeneous vs. inhomogeneous medium
% Per default, the AirtrafficNoise renderer computes the sound paths
% required for the auralization assuming a homogeneous medium, i.e. based
% on the well-known image source method. This approach is comparable to
% render two sound sources under free-field conditions and is therefore
% real-time capable (see www.virtualacoustics.org/VA/overview/#real-time-auralization-some-technical-notes).
% 
% In case of the inhomogeneous medium the sound paths are simulated using
% the Atmospheric Ray Tracing framework (www.virtualacoustics.org/GA/art/).
% The underlying simulation are computationally demanding. Thus, it is not
% possible to run one simulation for each audio block. In order to still be
% real-time capable, the simulations are scheduled into a separate thread
% and the results are interpolated using so-called propagation parameter
% histories (see www.virtualacoustics.org/VA/documentation/rendering/#propagation-parameter-histories).
% 
% The respective settings for the medium type, to schedule the simulations
% and use the propagation parameter histories are defined in the ini-file.
% Below, you can find the names of the ini-files used in this example. The
% following boolean can be used to switch between both configurations.
bInhomogeneous = false;

% For more information, also check the renderer's documentation on
% www.virtualacoustics.org/VA/documentation/rendering/#airtrafficnoise.

%% Configuration parameters (VACore.ini)
va_ini_homogeneous = 'VACore.AircraftHomogeneous.ini';
va_ini_inhomogeneous = 'VACore.AircraftInhomogeneous.ini';

%% Scene Parameters

% Receiver
receiver_position = [0 1.7 0];              % Center of coordinate system, height of 1.7m
receiver_view = [0 0 -1];                   % View: negative z (to the front)
receiver_up = [0 1 0];                      % Up: positive y

receiver_directivity = '$(DefaultHRIR)';    % Receiver directivity file (see VACore.AircraftHomogeneous.ini [Macros] section)

% Source
source_start_position = [-1000 1000 -500];  % From receiver: 1000m to the left, 1000m high and 500m away
source_view = [1 0 0];                      % View: in x direction, the direction of the trajectory
source_up = [0 1 0];                        % Up: positive y   
source_velocity = 84;                       % Velocity: 84m/s -> 302.4km/h

source_directivity = '';                    % Default empty string for no specific directivity.

signal_source_file = '$(AircraftNoise)';     % Signal source file (see [Macros] section in .ini file)

source_SPL = 130;                           % Source Sound Pressure Level in dB
source_SP = 10^(source_SPL/10)*1e-12;       % Conversion from SPL to SoundPower

% CAUTION : Changing source position or source SPL may result in high
% output levels. Be careful with you hearing when experimenting with these
% values.

% Scenario
%       General variables for the scenario
time_start = 0;                             % Time when simulation starts
time_end = 20;                              % Time when trajectory ends

%       Real-time rendering variables
motion_update_rate = 50;                    % How many times a second the positions are updated

deltaT = 1/motion_update_rate;
time_vector = (time_start:deltaT:time_end);

%% Source trajectory
source_trajectory = VATrajectory.CreateLinear( source_start_position, source_view, ...
    source_up, source_velocity, time_end, time_start );

[ aircraft_positions, ~ , ~ ] = source_trajectory.evaluate( time_vector );

%% Start VAServer and connect
va = VA;
if bInhomogeneous
    va.start_server( which(va_ini_inhomogeneous) );
else
    va.start_server( which(va_ini_homogeneous) );
end
va.connect('localhost');

%% Scene Setup
va.set_output_muted( true );

% Create virtual listener
L = va.create_sound_receiver( 'VA_listener' );
va.set_sound_receiver_position( L , receiver_position );
va.set_sound_receiver_orientation_view_up( L , receiver_view , receiver_up );

H = va.create_directivity_from_file( receiver_directivity );
va.set_sound_receiver_directivity( L , H );

% Create virtual source
X = va.create_signal_source_buffer_from_file( signal_source_file );
va.set_signal_source_buffer_playback_action( X, 'play' )
va.set_signal_source_buffer_looping( X, true );

S = va.create_sound_source( 'VA_Source' );
va.set_sound_source_sound_power( S , source_SP);
va.set_sound_source_position( S, source_start_position );

va.set_sound_source_signal_source( S, X );

%% De-/Activate turbulence
% va.set_global_auralization_mode('-TV') % Deactivate
va.set_global_auralization_mode('+TV') % Activate

%% Auralization loop
% This is an example on how to move a source or receiver synchronously using an update loop.
% See https://www.virtualacoustics.org/VA/documentation/scene/#high-performance-timer for more
% information. A similar implementation of this loop is executed when using the VAScenario class
% for the auralization (see VA_example_dynamic_scene_convenience_classes.m).
%
% Note, that in the beginning of the auralization either no sound output is
% generated or artifacts can occur. The first case is caused by the buffer
% used to delay the input signal based on the propagation delay (variable
% delay-line), which requires some time to be filled with data. Simply wait
% a few seconds, before running this section to avoid this.
% The artifacts are caused by the aircraft being a static object after the
% "Scene Setup" and suddenly starts moving once executing this section.
% This is of course physically incorrect and therefore leads to respective
% artifacts.

% Timer for motion updates
va.set_timer( deltaT );

va.set_output_muted( false );
for idx = 1:numel(time_vector)
    va.wait_for_timer( );
    va.set_sound_source_position( S, aircraft_positions(idx,:) )
end
va.set_output_muted( true );

%% Finalize and end example
va.shutdown_server
va.disconnect
