%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% VAEXAMPLE: RoomAcoustic Renderer    %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This example introduces the RoomAcoustics renderer. It simulates a 
% room impulse response using the RAVEN simulation backend.
% Note, that RAVEN is not publically available and thus this example only
% works with a special version of VA that includes RAVEN. However, RAVEN
% and the respective VA version are available for academic and research
% purposes (see [1] for more info).
% The documentation for the renderer can be found here: [2].

% In the background, the renderer uses the ITASimulationScheduler [3] to
% schedule the simulations. Individual simulations are scheduled for
% direct sound, early reflections and late reverberation. This allows
% updating the early parts of the RIR at a much higher rate than the late
% reverberation.

% You will learn how to:
%   - Use the RoomAcoustic Renderer
%   - Enable/Disable direct sound, early reflections and late reverberation

% The room model used is an L-shaped room like this (top town):
%       ________
%       |       |
%       |   S   |
% ______|       |
% |             |
% |  R          |
% |_____________|

% IMPORTANT: Make sure to run VAMatlab_setup.m before running this example

% References:
% [1] https://www.virtualacoustics.de/RAVEN/#access
% [2] https://www.virtualacoustics.de/VA/documentation/rendering/#roomacoustics-not-public
% [3] https://www.virtualacoustics.de/ITASimulationScheduler/

%% Side notes
% Currently, the renderer only supports binaural repoduction.

% In order to reduce the computational load, the RoomAcoustics renderer
% could also be configured to only handle direct sound and early
% reflections. Then, it can be combined with the BinauralArtificialReverb
% renderer which takes over the late reverberation. However, this is not in
% the scope of this example.

%% Related examples
% - VA_example_artificial_reverb.m

%% Configuration parameters (VACore.ini)
va_core_ini = 'VACore.RAVENRoomAcoustics.ini';
renderer_id = 'MyBinauralRoomAcoustics';
sampling_rate = 44100;

%% Additional configuration files
% Using the ITASimulationScheduler with RAVEN requires an additional
% configuration files.
% 
% First is the configuration of the scheduler itself. Here, it is defined
% what simulation backend is used and under what circumstances a simulation
% update is actually triggered. Here, the respective file is called
% VASchedulingRAVEN.json. The configuration itself is not an easy
% plug-and-play solution. It requires careful setup to deliver the desired
% result on the hardware at hand. Thus, careful study of the publications
% [4] and the documentation [5] is adviced when trying to adapt the configuration.
% 
% Additionally, RAVEN requires a configuration for the actual simulation.
% This especially includes the room geometry and material data. The
% respective file is ./RAVEN_data/example.rpf. For more information on
% configuring RAVEN please refer to the RAVEN documentation and tutorials.

% References:
% [4] https://www.virtualacoustics.de/ITASimulationScheduler/#scientific-publications
% [5] https://git.rwth-aachen.de/ita/itasimulationscheduler/-/blob/master/README.md

%% Switch current directory
% In order for all configuration and data files to be found, VA has to be
% called from this directory
cd( fileparts(which('VA_example_RAVEN_room_acoustics')) )

%% Start VAServer
va = VA;
va.start_server( which(va_core_ini) );

%% Connect and adjust output gain
va.connect( 'localhost' )
va.set_output_gain( .25 )

%% Setup source and receiver
% See VA_example_simple for more details.
% Note, that source and receiver have to be positioned inside the room!

X = va.create_signal_source_buffer_from_file( '$(DemoSound)' );
va.set_signal_source_buffer_playback_action( X, 'stop' )
va.set_signal_source_buffer_looping( X, true );

S = va.create_sound_source( 'VA_Source' );
va.set_sound_source_position( S, [ 10 1.7 -10 ] )

% Note, that we do not need an HRTF here since it is configured in the
% RAVEN configuration file (see RAVEN_data/example.rpf)
L = va.create_sound_receiver( 'VA_Listener' );
va.set_sound_receiver_position( L, [ 2.5 1.7 -2.5 ] )

va.set_sound_source_signal_source( S, X )

%% Check if direct sound, early reflections and diffuse decay were simulated
simulations = va.get_rendering_module_parameters(renderer_id, struct() )

% The field names follow the structure  if SRP_[sourceID]_[receiverID]_[RIR part].
% SRP stands for "Source Receiver Pair"
% And the RIR part can be one of three:
% - DS = Direct Sound
% - ER = Early Reflections
% - DD = Diffuse Decay
% The value of these represents the number of simulations that were
% completed so dar.

fn = fieldnames(simulations);
for k=1:numel(fn)
    if( str2double(simulations.(fn{k})) == 0 )
        error('Not all RIR parts were simulated. Wait for simulation to finish and run section again.')
    end
end

va.set_signal_source_buffer_playback_action( X, 'play' )

%% ENABLE / DISABLE DIRECT SOUND / EARLY REFLECTIONS / LATE REVERB %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Disable late reverberation (diffuse decay)
va.set_global_auralization_mode( '-DD' )
%% Disable early reflections
va.set_global_auralization_mode( '-ER' )

% You will notice, that you do not hear anything.

% This is correct, since the receiver and the source cannot see eachother.
% When we move the receiver to the right, this will change and the
% auralization will adapt accordingly.

%% Move the receiver
va.set_sound_receiver_position( L, [ 8 1.7 -2.5 ] )

%       ________
%       |       |
%       |   S   |
% ______|       |
% |             |
% |  R  -> R    |
% |_____________|

%% Disable direct sound
va.set_global_auralization_mode( '-DS' )
%% Enable direct sound
va.set_global_auralization_mode( '+DS' )
%% Enable early reflections
va.set_global_auralization_mode( '+ER' )
%% Enable late reverberation (diffuse decay)
va.set_global_auralization_mode( '+DD' )

%% Finalize and end example
va.shutdown_server
va.disconnect
