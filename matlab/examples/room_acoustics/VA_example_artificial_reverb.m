%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% VAEXAMPLE: Artificial Reverberation %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This example introduces the BinauralArtificialReverb renderer. It simulates an impulse response for the late
% reverberation using some basic information on the room, e.g. volume, surface area and frequency-dependent
% reverberation times. Note, that it does not consider the actual geometry or materials at the walls.
% For more information, see the respective documentation under:
% https://www.virtualacoustics.org/VA/documentation/rendering/#binauralartificialreverb

% Since this renderer does not consider direct sound or early reflections, it is a good approach to combine it
% with other renderers. In this example, it is combined with a binaural free-field renderer, which simulates the direct sound.

% You will learn how to:
%   - Configure the BinauralArtificialReverb renderer
%   - Read and adjust the specified reverberation times
%   - Enable/Disable direct sound and late reverberation using auralization modes

% IMPORTANT: Make sure to run VAMatlab_setup.m before running this example

%% Configuration parameters (VACore.ini)
va_core_ini = 'VACore.ArtificialReverb.ini';
direct_sound_renderer_id = 'MyBinauralFreeField';
artificial_reverb_renderer_id = 'MyBinauralArtificialReverb';
sampling_rate = 44100;

%% Start VAServer
va = VA;
va.start_server( which(va_core_ini) );

%% Use source and receiver setup from VA_example_simple
VA_example_simple


%% MODIFY REVERBERATION %%
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Read current reverberation times
reverbs = va.get_rendering_module_parameters('MyBinauralArtificialReverb', struct() )

%% Large reverberation time (church-like)
reverbs.room_reverberation_times = [ 8 4 2 ];
va.set_rendering_module_parameters( 'MyBinauralArtificialReverb', reverbs )

%% Default reverberation time (small room)
reverbs.room_reverberation_times = [ 1 0.71 0.3 ];
va.set_rendering_module_parameters( 'MyBinauralArtificialReverb', reverbs )

%% Medium room
reverbs.room_reverberation_times = 2; % -> factor of sqrt(2) 1 sqrt( 1/2 ) for low mid high bands
va.set_rendering_module_parameters( 'MyBinauralArtificialReverb', reverbs )
va.get_rendering_module_parameters('MyBinauralArtificialReverb', struct() ).room_reverberation_times


%% ENABLE / DISABLE DIRECT SOUND / LATE REVERB %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Disable direct sound
va.set_global_auralization_mode( '-DS' )
%% Enable direct sound
va.set_global_auralization_mode( '+DS' )

%% Disable late reverberation (diffuse decay)
va.set_global_auralization_mode( '-DD' )
%% Enable late reverberation (diffuse decay)
va.set_global_auralization_mode( '+DD' )

