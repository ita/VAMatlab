%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% VA EXAMPLE: JET ENGINE SIGNAL SOURCE %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% In this example, you will learn how to create and adjust a jet engine signal source
% (see https://www.virtualacoustics.org/VA/documentation/scene/#jet-engine)

% IMPORTANT: Make sure to run VAMatlab_setup.m before running this example

%% Start VAServer using default VACore.ini file
va = VA;
va.start_server( );

%% Connect
va.connect();
va.set_output_gain( .25 )

%% SCENE DEFINITION
%% Receiver
L = va.create_sound_receiver( 'listener' );
va.set_sound_receiver_position( L, [ 0 1.7 0 ] );
H = va.create_directivity_from_file( '$(DefaultHRIR)' );
va.set_sound_receiver_directivity( L, H );

%% Sound source
S = va.create_sound_source( 'jet engine' );
va.set_sound_source_position( S, [ 0 1.7 -300 ] ); %300m in front of receiver
sound_power_level = 130; %dB
va.set_sound_source_sound_power( S, 10^(sound_power_level/10) * 1e-12 );

%% Create jet engine signal and connect to source
% Signal source is created using special struct with `class` field and
% value 'jet_engine'
JE = va.create_signal_source_prototype_from_parameters( struct('class', 'jet_engine') );
va.set_sound_source_signal_source( S, JE );

%% SCENE ADAPTATION
%% Increase RMP over time
va.set_signal_source_parameters( JE, struct( 'rpm', 1500 ) )
pause( 2 )
va.set_signal_source_parameters( JE, struct( 'rpm', 1600 ) )
pause( 0.2 )
va.set_signal_source_parameters( JE, struct( 'rpm', 1700 ) )
pause( 0.2 )
va.set_signal_source_parameters( JE, struct( 'rpm', 1800 ) )
pause( 0.4 )
va.set_signal_source_parameters( JE, struct( 'rpm', 2100 ) )
pause( 1.4 )
va.set_signal_source_parameters( JE, struct( 'rpm', 3800 ) )
pause( 4 )
va.set_signal_source_parameters( JE, struct( 'rpm', 5000 ) )
