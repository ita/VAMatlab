# VAMatlab data

## License

The following files are licensed under Creative Commons BY-NC-SA 4.0 by the Institute for Hearing Technology and Acoustics (IHTA), RWTH Aachen University:

 - ITA_Artificial_Head_horizontal_front_left_44kHz_128.wav
 
For more information, higher resolutions for academic purposes and commercial use, please contact us.
