%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% VA EXAMPLE: AMBIENT SOUND  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% In this example a simple static scene will be set up using two different renderers : FreeField and AmbientMixer.
% The first will be used to renderer a target speaker talking at a specific position. The latter will add some
% ambient sound (background noise) using a mono signal.

% You will learn how to:
%   - Use multiple renderers in parallel
%   - Use sound sources which are specific to a single rendering module

% To check out the full configuration of the VAServer, open VACore.AmbientSound.ini file.
% Visit https://www.virtualacoustics.org/VA/documentation/rendering/#rendering-class-overview
% to learn more about the utilized rendering modules.

% IMPORTANT: Make sure to run VAMatlab_setup.m before running this example

%% Configuration parameters (VACore.ini)
va_ini_filename = 'VACore.AmbientSound.ini';
ambient_renderer_id = 'MyAmbientMixer';
freefield_renderer_id = 'MyBinauralFreeField';

%% Scene parameters

% Receiver
receiver_position = [0 1.7 0];          % Center of coordinate system, height of 1.7m
receiver_view = [0 0 -1];               % View: negative z (to the front)
receiver_up = [0 1 0];                  % Up: positive y

receiver_directivity = '$(DefaultHRIR)';

% Source
source_position = [0 1.7 -7];           % 1.7m high, 7m in front of receiver
source_view = [0 0 1];                  % View: positive z (to the back)
source_up = [0 1 0];                    % Up: positive y
source_directivity = '$(HumanDir)';  

talker_sound_file = '$(DemoSound)';

% Ambient noise
ambient_sound_file = '$(AmbientSound)'; % Ambient noise file (see VACore.AmbientSound.ini [Macros] section)


%% Start VAServer and connect
va = VA;
va.start_server( which(va_ini_filename) );
va.connect('localhost');

%% SCENE DEFINITION
%% Receiver
L = va.create_sound_receiver('VA_listener');
va.set_sound_receiver_position( L , receiver_position );
va.set_sound_receiver_orientation_view_up( L , receiver_view , receiver_up );

H = va.create_directivity_from_file( receiver_directivity );
va.set_sound_receiver_directivity( L , H );

%% Objects for AmbientMixer
% Renderers are referenced by there ID, which is specified in the configuration file.
% In this case, these are 'MyAmbientMixer' and 'VACore.AmbientSound.ini'.

% Signal gain for output of specific renderer
va.set_rendering_module_gain(ambient_renderer_id, 0.3);

% Create a sound source, which is only used by one specific renderer
ambient_sound_source = va.create_sound_source_explicit_renderer( ambient_renderer_id, 'VA_AmbientSound' );

% Signal source
ambient_signal = va.create_signal_source_buffer_from_file( ambient_sound_file );
va.set_signal_source_buffer_playback_action( ambient_signal, 'play' );
va.set_signal_source_buffer_looping( ambient_signal, true );

va.set_sound_source_signal_source( ambient_sound_source, ambient_signal );


%% Objects for FreeField renderer (binaural)
% Creation of the talking source with MyBinauralFreeField renderer.

va.set_rendering_module_gain( freefield_renderer_id, 0.4 );

talker_sound_source = va.create_sound_source_explicit_renderer( freefield_renderer_id, 'VA_Source' );
va.set_sound_source_position( talker_sound_source, source_position );
va.set_sound_source_orientation_view_up( talker_sound_source, source_view, source_up );

talker_signal = va.create_signal_source_buffer_from_file( talker_sound_file );
va.set_signal_source_buffer_playback_action( talker_signal, 'play' );
va.set_signal_source_buffer_looping( talker_signal, true );

va.set_sound_source_signal_source( talker_sound_source, talker_signal )

%% SCENE MODIFICATION
%% Pause ambient noise
va.set_signal_source_buffer_playback_action( ambient_signal , 'pause' );

%% Play ambient noise
va.set_signal_source_buffer_playback_action( ambient_signal , 'play' );

%% Mute talker
%You can either mute the overall output of the renderer or the sound source
va.set_rendering_module_muted( freefield_renderer_id, true );
va.set_sound_source_muted( talker_sound_source, true );

%% Unmute talker
va.set_rendering_module_muted( freefield_renderer_id, false );
va.set_sound_source_muted( talker_sound_source, false );

%% Finalize and end example
va.shutdown_server
va.disconnect
