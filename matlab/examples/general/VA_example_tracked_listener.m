%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% VA EXAMPLE : TRACKED LISTENER %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This example showcases how a tracking system can be connected with the
% receiver movement. In this way, the movement of the listener can directly
% be transferred to VA to be considered in the auralization.
% Generally, VAMatlab provides an interface to connect to an OptiTrack or
% Advanced Realtime Tracking (ART) system.

% In this example, you will learn how to:
%   - Learn the difference between virtual and real-world receiver pose
%   - Control the receiver movement using the tracker
%   - Start / stop tracking

%% Virtual vs. real-world pose
% Generally, it is possible to either control the virtual pose, the
% real-world pose or both with the tracking system. To understand the
% differences, please read the following part of VA's documentation:
% https://www.virtualacoustics.org/VA/documentation/scene/#virtual-vs-real-world-pose

%% Tracking system configuration
% In general, both OptiTrack and ART use a network protocol to send the
% tracking data from their "source" to the PC where it is being used.
% For OptiTrack, the "source" is the PC where Motive is running. For ART,
% the tracker itself is the "source". As a result, the IP addresses of
% these devices are required.
% In addition, OptiTrack requires the IP address of the reveiving
% PC as well.

% The following IPs have to be adapted to your setup!
tracking_source_ip = '192.168.78.1';
tracking_receiver_ip = '127.0.0.1'; % loopback IP address
tracking_type = 'OptiTrack';        % or 'ART'

%% Start VAServer and connect
va = VA;
va.start_server( ); % Uses default VACore.ini file
va.connect('localhost');

%% Init signal  and sound source
X = va.create_signal_source_buffer_from_file( '$(DemoSound)' );
va.set_signal_source_buffer_playback_action( X, 'play' );
va.set_signal_source_buffer_looping( X, true );

% Source position: front-right from center, 1.7m above ground
S = va.create_sound_source( 'VA_Source' );
va.set_sound_source_position( S, [ 2 1.7 -2 ] ); 
va.set_sound_source_signal_source( S, X );

%% Init tracked receiver / listener
L = va.create_sound_receiver( 'VA_Tracked_Listener' );

% Connect tracking data with receiver movement
va.set_tracked_sound_receiver( L )              % Virtual pose -> rendering modules
va.set_tracked_real_world_sound_receiver( L )   % Real-world pose -> reproduction modules (e.g. CTC / BinauralMixdown)

%% Start tracking for some seconds
va.connect_tracker( tracking_source_ip, tracking_receiver_ip, tracking_type )
% Observe how you can move the virtual sound receiver
for idx = 1:12
    current_position = va.get_sound_receiver_position( L );
    disp( current_position )
    pause( 1 )
end
va.disconnect_tracker

%% Finalize
va.shutdown_server( );
va.disconnect( );
