%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% VA EXAMPLE: DYNAMIC SCENE; CONVENIENCE CLASSES  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% In this example, a set of convenience classes for the auralization of dynamic scenarios are introduced.
% Using those classes, it is not required to implement the auralization loop handling e.g. the motion updates
% of sources and receiver anymore. Instead all relevant data is stored within a set of class objects and the
% actual auralization, including starting and closing the VAServer, is handled for you.
% This is showcased using a simple scenario with a person walking from left to right.

% You will learn how to:
%   - Use the convenience classes VATrajectory, VASource, VAReceiver and VAScenario.
%   - Run an real-time and offline auralization using the VAScenario class.
%   - Using multiple sound sources

% Note, that different ini-files are required for real-time and offline auralization, since for the latter a virtual
% sound card is used (see https://www.virtualacoustics.org/VA/documentation/recording/#offline-rendering-and-capturing).

% IMPORTANT: Make sure to run VAMatlab_setup.m before running this example

%% Configuration parameters (VACore.ini)
va_realtime_ini = 'VACore.DynamicScene.ini';
va_offline_recording_ini = 'VACore.DynamicScene.recording.ini';
sampling_rate = 44100;
block_size = 256;   %block size used in offline auralization (see VACore.DynamicScene.recording.ini [Audio driver] section)
renderer_id = 'MyBinauralFreeField';

%% Scene parameters
% ---Receiver ---
receiver_position = [0 1.7 0];              % Center of coordinate system, height of 1.7m
receiver_view = [0 0 -1];                   % View: negative z (to the front)
receiver_up = [0 1 0];                      % Up: positive y-direction
receiver_directivity = '$(DefaultHRIR)';    % Receiver directivity file (see VACore.DynamicScene.ini [Macros] section)

% ---Source---
%   Parameter used for trajectory generation
source_start_position = [-10 1.7 -5];       % From receiver: 10m to the left, 1.7m high and 5m away
source_view = [1 0 0];                      % View: in x-direction, the direction of the trajectory
source_up = [0 1 0];                        % Up: positive y-direction
source_velocity = 1;                        % Velocity: 1m/s

%   Other parameters
source_SPL = 100;                           % Source Sound Pressure Level in dB
source_SP = 10^(source_SPL/10)*1e-12;       % Conversion from SPL to sound power
source_directivity = '$(HumanDir)';         % Source directivity, (see VACore.DynamicScene.ini [Macros] section).
                                            % You can use an empty string '' (default) for no directivity.

% ---Signal source---
signal_source_file = '$(DemoSound)';        % Signal source file (see VACore.DynamicScene.ini [Macros] section)

% ---Scenario---
%   Time
time_start = 0;                             % Time when auralization starts
time_end = 20;                              % Time when auralization ends
motion_update_rate = 60;                    % How many times a second the positions are updated (real-time only)

%   Recording destination
examples_folder = fileparts( which('VA_example_dynamic_scene_convenience_classes') );
rendering_output_folder = fullfile( examples_folder, 'recordings' );
rendering_output_filename = 'dynamic_scene.wav';


%% CONVENIENCE CLASSES %%
%%%%%%%%%%%%%%%%%%%%%%%%%
%% VATrajectory class
% The VATrajectory class allows the defining the movement (translation and
% orientation) of sources and receiver during the scenario. It is given a
% set of position, view-vector and up-vector keys with respective time
% stamps. Then the trajectory can be interpolated to get data vectors with
% the required temporal resolution.

% Receiver trajectory
% Here, we use a static receiver, meaning position, view- and up-vector are
% constant. In this case, it is also allowed to only hand a single key
% although there are two time keys present (see view and up-vector).
% Since the receiver is static, it is also possible, to only give a single
% value for position, view, or up-vector
receiver_trajectory = VATrajectory( [time_start ; time_end], ...
    [receiver_position ; receiver_position], receiver_view,  receiver_up );

% Source Trajectory
% If you want to create a linear trajectory based on a starting position
% and a velocity, you can use the following function. In this case, the
% view vector is used as direction of movement and orientation is kept
% constant.
source_trajectory = VATrajectory.CreateLinear( source_start_position, source_view, ...
    source_up, source_velocity, time_end, time_start );


%% VASource and VAReceiver classes
% The auralization input data regarding sources and receiver is defined
% using those classes. This includes the trajectory and directivity. For
% the source, additionally the wav file used for the signal source and the
% sound power is specified.

receiver = VAReceiver( receiver_trajectory , receiver_directivity , 'VA_listener' );
source = VASource( source_trajectory, signal_source_file, source_SP, source_directivity , 'VA_source' );

%% VAScenario class
% This class represents the scenario being auralized. Here, you specify all
% settings relevant to the auralization. This includes the relevant
% configuration parameters as well as the ini-file for the VAServer
% application to start with.
% Afterwards, the auralization can be run with a single command specifying
% the sources, the receiver to be auralized as well as the duration either 
% in real-time or offline. This will also automatically start and close the
% VAServer for you.

%% REAL-TIME AURALIZATION %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Scenario for real-time auralization 
% See VACore.DynamicScene.ini for respective VA configuration

scenario_realtime = VAScenario();
scenario_realtime.sampling_rate = sampling_rate;           % Sampling rate: make sure it matches with the sampling rate in the .ini file
scenario_realtime.motion_update_rate = motion_update_rate; % How frequently the position is updated in real-time rendering                     
scenario_realtime.renderer_id = renderer_id;
scenario_realtime.va_ini_path = which( va_realtime_ini );

%% Run auralization
scenario_realtime.realtime_rendering( source, receiver, time_end );

%% OFFLINE AURALIZATION %%
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Scenario for offline auralization
% See VACore.DynamicScene.recording.ini for respective VA configuration

scenario_offline = VAScenario();
scenario_offline.sampling_rate = sampling_rate;                         % Sampling rate: make sure it matches with the sampling rate in the .ini file                       
scenario_offline.block_size = block_size;                               % Block size: make sure it matches with the BufferSize specified in the .ini file ([Audio driver] section)
scenario_offline.rendering_output_folder = rendering_output_folder;     % Where to store the recorded audio
scenario_offline.rendering_output_file = rendering_output_filename;     % Name of the respective audio file
scenario_offline.renderer_id = renderer_id;
scenario_offline.va_ini_path = which( va_offline_recording_ini );

%% Run auralization
scenario_offline.offline_rendering( source,receiver, time_end );

%% Listen to result
[rendered_audio, f_sampling] = audioread( fullfile( rendering_output_folder, rendering_output_filename ) );
player = audioplayer( rendered_audio, f_sampling );
play( player );


%% MULTIPLE SOUND SOURCES %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Add an aircraft as second source
% For simplicity, mirror original trajectory
source_mirrored_trajectory = VATrajectory( [time_start time_end],  source_trajectory.position([2 1],:), -source_view, source_up );
aircraft_source = VASource( source_mirrored_trajectory, '$(AircraftSound)', source_SP, '' , 'VA_aircraft_source' );

%% Real-time auralization
% Run the real-time auralization again but this time with two sources
scenario_realtime.realtime_rendering( [source aircraft_source], receiver, time_end );

%% Offline auralization
% Copy scenario and change destination for recorded file
[~, name, ext] = fileparts(rendering_output_filename);
scenario_offline_two_sources = scenario_offline;
scenario_offline_two_sources.rendering_output_file = [name '_multi_sources' ext];

scenario_offline_two_sources.offline_rendering( [source aircraft_source],receiver, time_end );
