%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% VA EXAMPLE: REAL-TIME RECEIVER ROTATION %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% When auralizing complex scenarios (e.g. considering complex propagation effects or many sources), the setup might
% not be real-time capable anymore. An approach to keep a certain degree of real-time interaction with the scene,
% namely the receiver rotation, uses a combination of offline and real-time processing:
% First, the scenario is rendered offline to record an ambisonics encoded signal. Then, this signal is used as input
% to a real-time auralization where the signal is decoded depending on the receiver orientation (e.g. to a binaural signal)
% 
% This example uses a very simple scenario using a single source (person walking from left to right) assuming free-field conditions.
% Note, that generally, it would be possible to render such a scenario directly in real-time, but we want to keep things as simple as
% possible here.

% You will learn how to:
%       - Use the ambisonics free-field renderer to create an ambisonics encoded signal (see
%       VACore.RealTimeReceiverRotation.recording.ini and
%       https://www.virtualacoustics.org/VA/documentation/rendering/#rendering-class-overview ).
%       - Make an auralization using an ambisonics encoded signal using the Ambient Mixer Renderer in combination with
%       the AmbisonicsBinauralMixdown.

% IMPORTANT: Make sure to run VAMatlab_setup.m before running this example

%% Configuration parameters (VACore.ini)
va_offline_recording_ini = 'VACore.RealTimeReceiverRotation.recording.ini';
offline_renderer_id = 'MyAmbisonicsFreeField';
offline_binaural_renderer_id = 'MyBinauralFreeField';
block_size = 256;

va_realtime_ini = 'VACore.RealTimeReceiverRotation.ini';
tracked_listener_id = 1;
reproduction_center_pos = [0 0 0];

sampling_rate = 44100;

%% Recording destination
folder_of_this_script = fileparts( which('VA_example_realtime_receiver_rotation.m') );
recording_folder = fullfile(folder_of_this_script, 'recordings');
ambisonics_recording_filename = 'ambisonics_ff_offline_recording.wav';
binaural_recording_filename = 'binaural_ff_offline_recording.wav';

%% Scene parameters
% Time
time_start = 0;                         % Time when simulation starts
time_end = 20;                          % Time when simulation ends
time_step = block_size/sampling_rate;   % Time step used in offline auralization
time_vector_offline = time_start:time_step:time_end;

% Receiver
receiver_position = [0 1.7 0];          % Center of coordinate system, height of 1.7m
receiver_view = [0 0 -1];               % View: negative z (to the front)
receiver_up = [0 1 0];                  % Up: positive y
receiver_directivity = '$(DefaultHRIR)';

% Source with linear trajectory (see below)
source_time_keys     = [time_start, (time_end-time_start)/2, time_end];
source_key_positions = [-10 1.7 0;      % Source start position: 10m to the left, 1.7m high
                       0  1.7 -5;       % Source middle position: 5m in front of receiver, 1.7m high
                       10 1.7 0];       % Source end position: 10m to the right, 1.7m high
source_view = [1 0 0];                  % View: in x-direction (same view for all three key positions of the trajectory)
source_up   = [0 1 0];                  % Up: positive y (same view for all three key positions of the trajectory)

source_directivity = '$(HumanDir)'; 
signal_source_file = '$(DemoSound)';


%% PART 1: CREATION OF AMBISONICS ENCODED AUDIO FILE %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Introduction
% The configuration file for the offline auralization is located in the
% same folder as this example and called 'VACore.RealTimeReceiverRotation.recording.ini'.
% We are using two free-field rendering modules. The first is for creating
% the desired ambisonics signals. The second one is optional and renders
% a binaural signal using the same scene. This file can be used to verify
% the result, as you can directly listen to it via headphones.
% Note, that the reproduction modules in this configuration are arbitrary,
% since we will record the output of the rendering modules. However, the
% number of rendering output channels and reproduction input channels must
% match!

%% Start VAServer and connect
va = VA;
va.start_server( which(va_offline_recording_ini) );
va.connect('localhost');

%% Scene initiation
% Receiver
L = va.create_sound_receiver('VA_listener');
va.set_sound_receiver_position( L , receiver_position );
va.set_sound_receiver_orientation_view_up( L , receiver_view , receiver_up );

H = va.create_directivity_from_file( receiver_directivity );
va.set_sound_receiver_directivity( L , H );

% Signal source
X = va.create_signal_source_buffer_from_file( signal_source_file );
va.set_signal_source_buffer_playback_action( X, 'play' );
va.set_signal_source_buffer_looping( X, true );

% Sound source
S = va.create_sound_source( 'VA_Source' );
va.set_sound_source_signal_source( S, X )

%% Renderer output recording destination
mStruct.RecordOutputFileName = ambisonics_recording_filename;
mStruct.RecordOutputBaseFolder = recording_folder;
va.set_rendering_module_parameters( offline_renderer_id, mStruct );

mStruct.RecordOutputFileName = binaural_recording_filename;
va.set_rendering_module_parameters( offline_binaural_renderer_id, mStruct );

%% Offline auralization
% In this example, the offline auralization of this dynamic scene is done
% manually. A more convenient way would be using the VAScenario class.
% Check out the example 'VA_example_dynamic_scene_convenience_classes' for
% more information.

% Linear interpolation of key positions using VATrajectory class
source_trajectory = VATrajectory(source_time_keys, source_key_positions, source_view, source_up);
[positions , views , ups ] = source_trajectory.evaluate( time_vector_offline );

manual_clock = 0;
num_time_steps = length( time_vector_offline );

h = waitbar( 0, 'Hold on, running auralization' );
for idxTime = 1:num_time_steps
    % Modify the scene
    va.set_sound_source_position( S, positions(idxTime,:) );

    % Increment core clock
    manual_clock = manual_clock + time_step;
    va.call_module( 'manualclock', struct( 'time', manual_clock ) );

    % Process audio chain by incrementing one block
    va.call_module( 'virtualaudiodevice', struct( 'trigger', true ) );

    if isvalid(h); waitbar( idxTime / num_time_steps, h ); end
end
if isvalid(h); close(h); end

%% Shutdown server to save result
% NOTE: recordings are only written into files, if server is shutdown
va.shutdown_server
va.disconnect


%% PART 2: AURALIZATION USING THE PREVIOUSLY RECORDED AMBISONICS SIGNAL %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Introduction
% In the real-time auralization, the previously recorded ambisonics signals
% are played using the AmbientMixer rendering module. For this renderer,
% the virtual positions of source and receiver are irrelevant as it simply
% plays given signals as is.
% The receiver rotation is handled using the BinauralAmbisonicsMixdown
% reproduction module. Generally, reproduction modules do not consider the
% virtual but the real-world receiver pose. Thus, the latter has to be
% adjusted in order to rotate the receiver.

% For more information on respective modules, see
% https://www.virtualacoustics.org/VA/documentation/rendering/#ambientmixer
% https://www.virtualacoustics.org/VA/documentation/reproduction/#binauralambisonicsmixdown

%% Start VAServer and connect
va = VA;
va.start_server( which(va_realtime_ini) );
va.connect('localhost');

%% Scene initiation
% Receiver
L = va.create_sound_receiver('VA_listener');
va.set_sound_receiver_position( L , receiver_position );
va.set_sound_receiver_orientation_view_up( L , receiver_view  , receiver_up);

% H = va.create_directivity_from_file( receiver_directivity );
% va.set_sound_receiver_directivity( L , H );

% Signal source
X = va.create_signal_source_buffer_from_file( fullfile( recording_folder, ambisonics_recording_filename ) );
va.set_signal_source_buffer_playback_action( X, 'play' )
va.set_signal_source_buffer_looping( X, true );

% Sound source
S = va.create_sound_source( 'VA_Source' );
va.set_sound_source_position( S, receiver_position + receiver_view * 5 ); % Place source 5m in front of receiver (but actually irrelevant)
va.set_sound_source_signal_source( S, X );


%% RECEIVER ROTATION
% In the next sections you can adjust the rotation of the receiver. There are examples of the four main directions (front,
% back, left, rigth). You can play around and change the direction during reproduction and listen to the changes.

%% Default rotation - facing front / same as during recording 
default_view = [0 0 -1];
default_up = [0 1 0];
va.set_sound_receiver_real_world_position_orientation_view_up( tracked_listener_id, reproduction_center_pos, default_view, default_up );

%% Rotate receiver - facing back:
va.set_sound_receiver_real_world_position_orientation_view_up( tracked_listener_id , reproduction_center_pos, [0 0 1], default_up );

%% Rotate receiver - facing left:
va.set_sound_receiver_real_world_position_orientation_view_up( tracked_listener_id , reproduction_center_pos, [-1 0 0], default_up );

%% Rotate receiver - facing right:
va.set_sound_receiver_real_world_position_orientation_view_up( tracked_listener_id , reproduction_center_pos, [1 0 0], default_up );


%% STOP / PAUSE / PLAY / MUTE SCENE (via signal source)
% Using this approach, the scene can easily be stopped / paused by
% modifying the driving signal source. Similarly, the scene can be muted
% through the respective sound source.
%% Stop
va.set_signal_source_buffer_playback_action( X, 'stop' )
%% Pause
va.set_signal_source_buffer_playback_action( X, 'pause' )
%% Play
va.set_signal_source_buffer_playback_action( X, 'play' )
%% Mute
va.set_sound_source_muted( S, true )
%% Unmute
va.set_sound_source_muted( S, false )


%% FINALIZE
va.shutdown_server
va.disconnect


%% FINAL REMARKS %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% As introduced earlier, a very simple scenario was used to showcase how
% to establish the auralization of computationally complex scenario
% considering real-time receiver orientation.
% Generally, the number of sound sources is not restricted. By adding
% sound sources to Part I, their contribution will be added to the recorded
% file. Thus, the individual source contributions cannot be separated
% anymore.
% If you want to control each source individually, you must render one
% offline scenario for each sound source and store the results in
% individual files. Then, in Part II, you must create one signal and sound
% source for each file, repectively. This allows, e.g., muting the
% contribution of certain sound sources.
