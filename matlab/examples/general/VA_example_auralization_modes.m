%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% VA EXAMPLE: AURALIZATION MODES  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% In this example, you learn to diable/enable certain sound propagation effects in real-time using so-called auralization modes.
% You can find detailed information about auralization modes in the documentation:
% https://www.virtualacoustics.org/VA/documentation/control/#global-auralization-mode

% You will learn how to:
%   - Check which auralization modes are currently active.
%   - Activate and deactivate auralization modes one by one and in groups.

% Here, a simple, static scene with a single source and receiver is used. The source represents a jet engine.
% The utilized rendering configuration considers the following sound propagation aspects
% - direct sound
% - ground reflection
% - air attenuation
% - turbulence-induced amplitude modulations

% IMPORTANT: Make sure to run VAMatlab_setup.m before running this example

%% Configuration parameters (VACore.ini)
va_ini_filename = 'VACore.AuralizationModes.ini';

%% Scene parameters

% Receiver
receiver_position = [0 1.7 0];              % Center of coordinate system, height of 1.7m
receiver_view = [0 0 -1];                   % View: negative z (to the front)
receiver_up = [0 1 0];                      % Up: positive y

receiver_directivity = '$(DefaultHRIR)';    % Receiver directivity file (See VACore.AuralizationModes.ini [Macros] section)

% Source
source_position = [0 3 -1000];                  % 3m high, 1000m in front of receiver
source_SPL = 130;                               % Source Sound Pressure Level in dB
source_sound_power  = 10^(source_SPL/10)*1e-12; % Conversion from SPL to SoundPower

% CAUTION : Changing source position or source SPL may result in high output levels. Be careful with you hearing when experimenting with
% these values.

%% Related examples
% This example uses a jet engine signal source which is introduced in:
% - VA_example_signal_source_jet_engine.m

%% Server Startup
va = VA;
va.start_server( which(va_ini_filename) );
va.connect('localhost');

%% SCENE INITIATION
L = va.create_sound_receiver( 'VA_listener' );

va.set_sound_receiver_position( L , receiver_position );
va.set_sound_receiver_orientation_view_up( L , receiver_view , receiver_up );

H = va.create_directivity_from_file( receiver_directivity );
va.set_sound_receiver_directivity( L , H );

S = va.create_sound_source( 'VA_jet_engine' );
va.set_sound_source_position( S, source_position );
va.set_sound_source_sound_power( S, source_sound_power );

% For more information on jet engine signal source, see VA_example_signal_source_jet_engine.m
JE = va.create_signal_source_prototype_from_parameters( struct('class', 'jet_engine') );
va.set_sound_source_signal_source( S, JE );

%% AURALIZATION MODE MODIFICATION
%% Read currently active auralization modes
va.get_global_auralization_mode

%% Disable Turbulence
va.set_global_auralization_mode('-TV')

%% Disable (early) reflections
% For the utilized renderer, this is the ground reflection
va.set_global_auralization_mode('-ER')

%% Disable air (medium) attenuation
va.set_global_auralization_mode('-MA')

%% Read auralization modes again
% Check that the disabled modes do not appear in the list of active auralization modes anymore
va.get_global_auralization_mode

%% Enable turbulence / ground reflection / air attenuation
% Enable Turbulence, Ground Reflection and Air Attenuation again
va.set_global_auralization_mode('+TV, +ER, +MA')

% See the list of auralization modes
va.get_global_auralization_mode

%% Finalize and end example
va.shutdown_server
va.disconnect
