%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% VA EXAMPLE: GENERIC FIR %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% In some situations, it might be required to apply user-defined FIR filters in an auralization,
% e.g. from measurements or external simulations. This can be achieved using the GenericFIR renderer
% (www.virtualacoustics.org/VA/documentation/rendering/#genericfir).
% 
% You will learn how to:
%   - Setup the GenericFIR renderer for real-time auralization
%   - Apply user-defined FIR filters for a specific source-receiver pair
%   - Apply FIRs from .wav files

% Note, generally it is also possible to also change the propagation delay
% to apply the Doppler shift with this renderer. However, this requires an
% update rate corresponding to the audio block rate. This can usually not
% be achieved from Matlab. See VA_example_generic_fir_dynamic.m for an
% offline example with a dynamic scene.

% IMPORTANT: Make sure to run VAMatlab_setup.m before running this example

%% Configuration parameters (VACore.ini)
va_ini_filename = 'VACore.GenericFIR.ini';
renderer_id = 'MyGenericFIR';

%% FIR filter data for two-channels
% Note, the minimum filter length is 2 samples
max_amplitude = 0.25;
neutral_fir = [max_amplitude max_amplitude; 0 0];            % 2-channel dirac, filter length: 2 samples
stereo_panning_fir = [max_amplitude 0.5*max_amplitude; 0 0]; % Stereo-filter panning to the left

hrir = audioread( which('ITA_Artificial_Head_horizontal_front_left_44kHz_128.wav') );
freefield_ir_8m = hrir / 8;                                  %HRIR (front-left) multiplied with 1/r factor for 8m distance

binaural_room_ir_file = '$(DemoBRIR)';

%% Start VAServer and connect
va = VA;
va.start_server( which(va_ini_filename) );
va.connect('localhost');

%% SCENE DEFINITION
%% Excitation signal
X = va.create_signal_source_buffer_from_file( '$(DemoSound)' );
va.set_signal_source_buffer_playback_action( X, 'play' );
va.set_signal_source_buffer_looping( X, true );

%% Source and receiver
% Note, that position of source and receiver have no influence on the
% output of this renderer. Nevertheless, they must be set so that, source
% and receiver become "valid".

L = va.create_sound_receiver( 'VA_Listener' );
va.set_sound_receiver_position( L, [ 0 1.7 0 ] );

S = va.create_sound_source( 'VA_Source' );
va.set_sound_receiver_position( L, [ 0 1.7 -2 ] );
va.set_sound_source_signal_source( S, X );

%% SCENE MODIFICATION %%
%%%%%%%%%%%%%%%%%%%%%%%%
%% EXCHANGING FIR FILTERS
% The FIR filter is exchanged using the set_rendering_module_parameters()
% function and a struct with special field names.
%% Preparing struct to send FIR
update_struct = struct();
update_struct.source = S;
update_struct.receiver = L;

%% Set neutral filter
update_struct.ch1 = neutral_fir( :, 1 );
update_struct.ch2 = neutral_fir( :, 2 );
va.set_rendering_module_parameters( renderer_id, update_struct );

%% Stereo left panning
update_struct.ch1 = stereo_panning_fir( :, 1 );
update_struct.ch2 = stereo_panning_fir( :, 2 );
va.set_rendering_module_parameters( renderer_id, update_struct );

%% Stereo right panning
update_struct.ch1 = stereo_panning_fir( :, 2 );
update_struct.ch2 = stereo_panning_fir( :, 1 );
va.set_rendering_module_parameters( renderer_id, update_struct );

%% Free-field IR (HRTF) - front left
update_struct.ch1 = freefield_ir_8m( :, 1 );
update_struct.ch2 = freefield_ir_8m( :, 2 );
va.set_rendering_module_parameters( renderer_id, update_struct );

%% USING FIR WAV FILES
%% Measured BRIR
update_struct = struct();
update_struct.source = S;
update_struct.receiver = L;
update_struct.filepath = binaural_room_ir_file;
va.set_rendering_module_parameters( renderer_id, update_struct );
