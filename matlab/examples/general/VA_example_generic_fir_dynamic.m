%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% VA EXAMPLE: GENERIC FIR DYNAMIC %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% In this example, a dynamic scene will be rendered based on user-defined FIR filters and
% propagation delays. To understand the basics of the utilized GenericFIR renderer
% (www.virtualacoustics.org/VA/documentation/rendering/#genericfir), it is recommended to
% check out the example VA_example_generic_fir.m first.
% 
% In this example, you will learn how to:
%   - Setup the GenericFIR renderer for offline auralization
%   - Apply user-defined FIR filters and propagation delays in an update loop

% IMPORTANT: Make sure to run VAMatlab_setup.m before running this example

%% Related examples
% Before running this tutorial, it is recommended to check out:
% - VA_example_generic_fir.m

%% Configuration parameters (VACore.ini)
va_ini_filename = 'VACore.GenericFIR.offline.ini';
renderer_id = 'MyGenericFIR';
sampling_rate = 44100;
block_size = 256;

%% Output file
examples_folder = fileparts( which('VA_example_generic_fir_dynamic.m') );
rendering_output_folder = fullfile( examples_folder, 'recordings' );
rendering_output_filename = 'generic_fir_dynamic_scene.wav';

%% Scene parameters
receiver_position = [0 1.7 0];      % Receiver position
source_center_position = [0 1.7 -2];% Center of source trajectory, 2m in front of receiver
souce_delta_x = 30;                 % Delta in x position from center (to left and right)
velocity = 40 / 3.6;                % Source velocity = 12 km/h -> m/s
speed_of_sound = 343;

fir_base_data = [1 1; 0 0];         % 2-channel dirac. Gain will be changed later according to 1/r law

%% Calculate distances and propagation delays for each audio block
t_max = 2*souce_delta_x / velocity;
delta_t = block_size / sampling_rate;
num_audio_blocks = ceil( t_max / delta_t );
source_x = linspace(-1, 1, num_audio_blocks)' * [souce_delta_x 0 0];
source_positions = source_center_position + source_x;

distances = vecnorm(source_positions - receiver_position, 2, 2);
propagation_delays = distances / speed_of_sound;

%% START VA AND INIT SCENE %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Start VAServer and connect
va = VA;
va.start_server( which(va_ini_filename) );
va.connect('localhost');

%% Set recording destination
recording_settings.RecordOutputFileName = rendering_output_filename;
recording_settings.RecordOutputBaseFolder = rendering_output_folder;
va.set_rendering_module_parameters( renderer_id, recording_settings );

%% Excitation signal
% Using jet engine here to have a continous signal with broadband component
JE = va.create_signal_source_prototype_from_parameters( struct('class', 'jet_engine') );
va.set_signal_source_parameters( JE, struct( 'rpm', 1200 ) )

%% Source and receiver
% Note, that position of source and receiver have no influence on the
% output of this renderer. Nevertheless, they must be set so that, source
% and receiver become "valid".

L = va.create_sound_receiver( 'VA_Listener' );
va.set_sound_receiver_position( L, [ 0 1.7 0 ] );

S = va.create_sound_source( 'VA_Source' );
va.set_sound_receiver_position( L, [ 0 1.7 -2 ] );
va.set_sound_source_signal_source( S, JE );

%% OFFLINE AURALIZATION %%
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Updating FIR and propagation delay within loop
update_struct = struct();
update_struct.source = S;
update_struct.receiver = L;

manual_clock = 0;
va.set_core_clock( 0 );
for idx = 1:numel(propagation_delays)
    % Update FIR and propagation delay
    update_struct.ch1 = fir_base_data(:,1) / distances(idx);
    update_struct.ch2 = fir_base_data(:,2) / distances(idx);
    update_struct.delay = propagation_delays(idx);
    va.set_rendering_module_parameters( renderer_id, update_struct );

    % Increment clock and render audio block
    manual_clock = manual_clock + delta_t;
    va.set_core_clock( manual_clock );
    va.call_module( 'virtualaudiodevice', struct( 'trigger', true ) );
end

%% Shutdown VA to store file
va.shutdown_server
va.disconnect
pause(2)

%% PLAYBACK RESULTS %%
%%%%%%%%%%%%%%%%%%%%%%
[rendered_audio, Fs] = audioread( fullfile( rendering_output_folder, rendering_output_filename ) );
player = audioplayer( rendered_audio, Fs );
play( player );
