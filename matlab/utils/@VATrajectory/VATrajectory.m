classdef VATrajectory < handle

    properties(SetAccess = private, GetAccess = public)
        time = [];      %Time data keys [Nx1]
        position = [];  %Position data keys [Nx3]
        view = [];      %View vector data keys [Nx3]
        up = [];        %Up vector data keys [Nx3]
    end
    properties(Access = public)
        default_interpolation_method (1,:) char {mustBeMember(default_interpolation_method,["linear","nearest","next", "previous", "pchip", "cubic", "v5cubic", "makima", "spline"])} = 'linear'; %Default interpolation method
    end

    %% Constructors
    methods
        function obj = VATrajectory(time_stamps, keys_position, keys_view, keys_up)
            %Default constuctor using time stamps and values for position, view and up vectors
            % Generally, number of keys should match the time stamps. However,
            % using a single value, a parameter will be set constant (typically
            % view and up).
            % If view and up vectors are not specified, default values
            % (view = [0 0 -1]; up = [0 1 0]) are used.
            arguments
                time_stamps (:,:) {mustBeVector, mustBeNumeric, mustBeReal}
                keys_position (:,3) {mustBeNumeric, mustBeReal}
                keys_view (:,3) {mustBeNumeric, mustBeReal} = [0 0 -1];
                keys_up (:,3) {mustBeNumeric, mustBeReal} = [0 1 0];
            end

            nKeys = numel(time_stamps);
            assert(size(keys_position, 1) == 1 || size(keys_position, 1) == nKeys, 'position data must be an Nx3 or 1x3 matrix, N referring to the number of time stamps');
            assert(size(keys_view, 1) == 1 || size(keys_view, 1) == nKeys, 'view vector data must be an Nx3 or 1x3 matrix, N referring to the number of time stamps');
            assert(size(keys_up, 1) == 1 || size(keys_up, 1) == nKeys, 'up vector data must be an Nx3 or 1x3 matrix, N referring to the number of time stamps');

            if isrow(time_stamps)
                time_stamps = time_stamps';
            end

            obj.time = time_stamps;
            obj.position = keys_position;
            obj.view = keys_view;
            obj.up = keys_up;
        end
    end

    methods(Static = true)
        function obj = CreateLinear(start_position, view, up, velocity, t_end, t_start)
            %Creates a linear trajectory using a constant orientation, start position and velocity.
            arguments
                start_position (1,3) {mustBeNumeric,mustBeReal,mustBeVector}
                view (1,3) {mustBeNumeric,mustBeReal,mustBeVector}
                up (1,3) {mustBeNumeric,mustBeReal,mustBeVector}
                velocity (1,1) {mustBeNumeric,mustBeReal}
                t_end (1,1) {mustBeNumeric,mustBeReal}
                t_start (1,1) {mustBeNumeric,mustBeReal} = 0
            end
            end_position = start_position + velocity * view * (t_end-t_start);

            time = [t_start, t_end];
            positions = [start_position; end_position];

            obj = VATrajectory(time, positions, view, up);
        end

        trajectory = ImportUnrealSequencer(filename, idxSequencerTrack, coordsystem)

        
        function obj = CreateLinearUniformlyAccelerated(start_position, view, up, velocity, acceleration, t_end, motion_rate, t_start )
            % Creates a linear trajectory with uniform accelartion using a
            % constant orientation, start, position, velocity, acceleration
            % and a motion update rate [Hz]

            arguments
                start_position (1,3) {mustBeNumeric,mustBeReal,mustBeVector}
                view (1,3) {mustBeNumeric,mustBeReal,mustBeVector}
                up (1,3) {mustBeNumeric,mustBeReal,mustBeVector}
                velocity (1,1) {mustBeNumeric,mustBeReal}
                acceleration (1,1) {mustBeNumeric, mustBeReal}
                t_end (1,1) {mustBeNumeric,mustBeReal}
                motion_rate (1,1) {mustBeNumeric,mustBeReal} = 50
                t_start (1,1) {mustBeNumeric,mustBeReal} = 0
            end

            time = (t_start : 1/motion_rate : t_end)';

            t    = time - t_start;
            positions = start_position + ...    % Original position
                velocity*t * view +...          % Start velocity contribution
                0.5*acceleration*t.^2 * view;   % Acceleration contribution

            obj = VATrajectory(time, positions, view, up);
        end

    end

    %% Concatenation
    methods
        function new_trajectory = concatenate(obj, other_trajectory)
            % Creates new trajectory concatenating this and given trajectory.
            %   Time keys must be in ascending order.
            arguments
                obj (1,1) VATrajectory
                other_trajectory (1,1) VATrajectory
            end

            assert(obj.time(end) <= other_trajectory.time(1), 'Trajectories must have ascending time stamps.')
            %Trajectories have the same end/start point?
            if obj.time(end) == other_trajectory.time(1)
                assert(...
                isequal(obj.position(end,:), other_trajectory.position(1,:)) &&...
                isequal(obj.view(end,:), other_trajectory.view(1,:)) &&...
                isequal(obj.up(end,:), other_trajectory.up(1,:)),...
                'Position, view and up vector at overlapping time stamp must be identical');
            end

            keys_time = [obj.time; other_trajectory.time];
            keys_position = VATrajectory.concatenate_3D_data(obj.time, obj.position, other_trajectory.time, other_trajectory.position);
            keys_view = VATrajectory.concatenate_3D_data(obj.time, obj.view, other_trajectory.time, other_trajectory.view);
            keys_up = VATrajectory.concatenate_3D_data(obj.time, obj.up, other_trajectory.time, other_trajectory.up);

            % Trajectories have the same end/start point? => Remove duplicated sample
            if obj.time(end) == other_trajectory.time(1)
                [keys_time, idx_unique] = unique(keys_time);
                if size(keys_position,1) > 1
                    keys_position = keys_position(idx_unique, :);
                end
                if size(keys_view,1) > 1
                    keys_view = keys_view(idx_unique, :);
                end
                if size(keys_up,1) > 1
                    keys_up = keys_up(idx_unique, :);
                end
            end

            new_trajectory = VATrajectory(keys_time, keys_position, keys_view, keys_up);
            new_trajectory.default_interpolation_method = obj.default_interpolation_method;
        end
        function new_trajectory = plus(obj, other_trajectory)
            % Concatenates trajectories (see concatenate())
            new_trajectory = concatenate(obj, other_trajectory);
        end
    end

    %% Interpolation
    methods
        function [position, view, up] = evaluate(obj, time_vector, interpolation_method)
            %Evaluates the trajectory for given time stamps.
            % Optionally the interpolation method can be specified.
            arguments
                obj
                time_vector (:,:) {mustBeNumeric,mustBeReal,mustBeVector}
                interpolation_method (1,:) char = 'default'
            end
            if strcmp(interpolation_method, 'default')
                interpolation_method = obj.default_interpolation_method;
            end
            position = obj.evaluate_3D_data(obj.position, time_vector, interpolation_method);
            view = obj.evaluate_3D_data(obj.view, time_vector, interpolation_method);
            up = obj.evaluate_3D_data(obj.up, time_vector, interpolation_method);
        end
    end
    methods(Access = private)
        function out = evaluate_3D_data(obj, data_vector, new_time_vector, interpolation_method)
            if (size(data_vector, 1) == 1)
                out = repmat(data_vector, numel(new_time_vector), 1);
            else
                out = interp1(obj.time, data_vector, new_time_vector, interpolation_method);
            end
        end
    end
    methods(Access = private, Static = true)
        function out = concatenate_3D_data(time1, data1, time2, data2)
            % Constant and equal data for both trajectories
            if size(data1,1) == 1 && size(data2,1) == 1 && isequal(data1, data2)
                out = data1;
                return;
            end
            if size(data1,1) == 1
                data1 = repmat(data1, numel(time1), 1);
            elseif size(data2,1) == 1
                data2 = repmat(data2, numel(time2), 1);
            end
            out = [data1; data2];
        end
    end

end