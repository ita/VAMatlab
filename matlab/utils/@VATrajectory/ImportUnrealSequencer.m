function trajectory = ImportUnrealSequencer(filename, idxSequencerTrack, coordsystem)

%% Input checks
arguments
    filename {mustBeFile}
    idxSequencerTrack (1,1) {mustBeInteger, mustBePositive} = 1
    coordsystem (1,:) char {mustBeMember(coordsystem,["opengl","matlab"])}   = 'opengl'
end

%% Beta warning
warning('VATrajectory.ImportUnrealSequencer(): This function is still at a beta version and might not provide proper results.');

%% Load unreal data
file_string = fileread(filename);

[time_keys, pos_x] = read_keys(file_string, 'Translation\(0\)', idxSequencerTrack);
[~, pos_y] = read_keys(file_string, 'Translation\(1\)', idxSequencerTrack);
[~, pos_z] = read_keys(file_string, 'Translation\(2\)', idxSequencerTrack);

[~, rot_x] = read_keys(file_string, 'Rotation\(0\)', idxSequencerTrack);
[~, rot_y] = read_keys(file_string, 'Rotation\(1\)', idxSequencerTrack);
[~, rot_z] = read_keys(file_string, 'Rotation\(2\)', idxSequencerTrack);

pos = [pos_x, pos_y, pos_z];
rot = [rot_x, rot_y, rot_z];

%% Convert to requested coordinates
[view, up] = va_unreal_rotation2viewup(rot);

switch (coordsystem)
    case 'opengl'
        pos = va_unreal2openGL(pos);
        view = va_unreal2openGL(view);
        up = va_unreal2openGL(up);
    case 'matlab'
        pos = va_unreal2matlab(pos);
        view = va_unreal2matlab(view);
        up = va_unreal2matlab(up);
    otherwise
        error('Unknown coordinate system')
end

%% Build trajecory
trajectory = VATrajectory(time_keys, pos, view, up);


function [time_keys, data_keys] = read_keys(file_string, data_pattern, idxSequencerTrack)

strFullData = extract_data_lines(file_string, data_pattern, idxSequencerTrack);

tick_resolution = get_tick_resolution(strFullData);

strTime = extract_time_string(strFullData);
strValues = extract_values_string(strFullData);

time_keys = extract_values(strTime) / tick_resolution;
data_keys = extract_values(strValues);

function data_line = extract_data_lines(str, data_pattern, idxOccurance)
%data_pattern , e.g. Translation\(0\)
regex_pattern = [data_pattern '\S*\TickResolution=\(Numerator=\d*?\)\)'];
[startIdx, endIdx] = regexp(str,regex_pattern, 'start');

data_line = str(startIdx(idxOccurance) : endIdx(idxOccurance));

function str_out = extract_time_string(str)
[idxStart, idxEnd] = regexp(str, 'Times=\(\(\S*?\)\)');
str_out = str(idxStart(1) : idxEnd(1));

function str_out = extract_values_string(str)
[idxStart, idxEnd] = regexp(str, 'Values=\(\(\S*?\)\),DefaultValue');
str_out = str(idxStart(1) : idxEnd(1));

function tick_resolution = get_tick_resolution(str)
str_tick_res = regexp(str, 'TickResolution=\(Numerator=\d*?\)', 'match');
str_tick_res = str_tick_res{1};
tick_resolution = str2double( str_tick_res(27:end-1) );

function data_array = extract_values(str)
strings = regexp(str, 'Value=\S*?[\),]', 'match'); %TODO: Komma or )
data_array = zeros(numel(strings), 1);
for idx = 1:numel(strings)
    data_array(idx) = str2double(strings{idx}(7:end-1));
end