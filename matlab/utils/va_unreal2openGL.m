function vecOut = va_unreal2openGL(vecIn, scaleFactor)
%VA_UINREAL2MATLAB converts either an Nx3 cartesian coordinates matrix or
%an itaCoordinates object (ITA-Toolbox) from the left-handed Unreal world 
%to the right-handed OpenGL coordinate system. Per default, also scales the
%vector lengths since Unreal uses cm as units instead of m.
%
%Transformation between coordinate systems are chosen that the view / up
%vectors do not change with respect to the respective default orientation:
%
%          MATLAB               OpenGL                    Unreal
%   
%           (+Z)                 (+Y)                       (+Z)
%             |                    |                         |
%             |                    |                         |
%    (+Y) - - .                    . - - (+X)                . - - (+Y)
%            /                    /                         /
%           /                    /                         /
%        (-X)                 (+Z)                      (-X)

if (isa(vecIn, 'itaCoordinates'))
    vecIn = vecIn.cart;
end
if nargin < 2; scaleFactor = 0.01; end
assert(size(vecIn,2)==3, 'Input has to be Nx3 matrix or itaCoordinates object.')
assert(isscalar(scaleFactor) && isnumeric(scaleFactor), 'Second input must be a scaling factor')

MATRIX_UNREAL2OPENGL= [0 0 -1; 1 0 0; 0 1 0];
vecOut = vecIn * MATRIX_UNREAL2OPENGL * scaleFactor;