function [view, up] = va_unreal_rotation2viewup(rotIn)
%VA_UNREAL_ROTATION2VIEWUP converts euler angles from Unreal rotation
%objects (degrees) to view and up vectors (left-handed Unreal world
%coordinate system).
% Input must be a Nx3 rotation matrix or an itaCoordinates
% object (ITA-Toolbox).
%
%          MATLAB               OpenGL                    Unreal
%   
%           (+Z)                 (+Y)                       (+Z)
%             |                    |                         |
%             |                    |                         |
%    (+Y) - - .                    . - - (+X)                . - - (+Y)
%            /                    /                         /
%           /                    /                         /
%        (-X)                 (+Z)                      (-X)

if (isa(rotIn, 'itaCoordinates'))
    rotIn = rotIn.cart;
end
assert(size(rotIn,2)==3, 'Input has to be Nx3 matrix or itaCoordinates object.')

yaw = rotIn(:,3);
pitch = rotIn(:,2);
roll = rotIn(:,1);

[view, up] = lefthanded_yawpitchroll2viewup( [yaw pitch roll] );


function [view, up] = lefthanded_yawpitchroll2viewup( ypr_deg )

% Because of left-handed coordinate system, pitch rotation must be switched
ypr_deg(:,2) = -ypr_deg(:,2);

view = zeros( size(ypr_deg) );
up = zeros( size(ypr_deg) );
cos_values = cosd(ypr_deg);
sin_values = sind(ypr_deg);


view(:,1) = cos_values(:,2).*cos_values(:,1);
view(:,2) = cos_values(:,2).*sin_values(:,1);
view(:,3) = -sin_values(:,2);


up(:,1) = cos_values(:,3).*sin_values(:,2).*cos_values(:,1) + sin_values(:,3).*sin_values(:,1);
up(:,2) = cos_values(:,3).*sin_values(:,2).*sin_values(:,1) - sin_values(:,3).*cos_values(:,1);
up(:,3) = cos_values(:,3).*cos_values(:,2);
