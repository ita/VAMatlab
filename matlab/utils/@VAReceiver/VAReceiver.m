classdef VAReceiver < handle

    properties
        name (1,:) char = '';                               %Name representing the source (optional)
        trajectory (:,1) VATrajectory = VATrajectory.empty; %Motion trajectory used during rendering process [VATrajectory]
        hrtf (1,:) char = '';                               %HRTF (daff) file [string]
    end

    methods
        function obj = VAReceiver(trajectory, hrtf, name)
            arguments
                trajectory (:,1) VATrajectory = VATrajectory.empty
                hrtf (1,:) char = ''
                name (1,:) char = ''
            end
            obj.trajectory = trajectory;
            obj.hrtf = hrtf;
            obj.name = name;
        end
        
        function set.trajectory(obj, value)
            assert(numel(value) <= 1, 'trajectory can only be a scalar or empty')
            obj.trajectory = value;
        end
        function set.hrtf(obj, value)
            assert(isempty(value) || startsWith(value, '$(') || endsWith(value, '.daff') , '"hrtf" must be of a filename to a .daff file or empty');
            obj.hrtf = value;
        end
    end
end