classdef VAScenario

properties
    sampling_rate (1,1) {mustBeNumeric, mustBeReal} = 44100;    %Sampling rate
    block_size (1,1) {mustBeNumeric, mustBeReal} = 1024;        %Audio block size
    motion_update_rate (1,1) {mustBeNumeric, mustBeReal} = 50;  %Update rate for motion used in real-time rendering
    preprocessing_time (1,1) {mustBeNumeric, mustBeReal} = 0;   %Preprocessing time in offline rendering used to fill VDL buffers
    
    renderer_id {mustBeText} = '';                              %ID of rendering module(s) [string or cell string]
    rendering_output_folder {mustBeText} = '';                  %Folder used to store output stream of renderer(s) [string or cell string]
    rendering_output_file {mustBeText} = '';                    %WAVE file to write output stream of renderer(s) [string or cell string]
    renderering_init_parameters (1,1) struct = struct();        %VA parameter struct to initialize rendering module before rendering process
    source_receiver_init_parameters (1,1) struct = struct();    %VA parameter struct to initialize parameters related to source-receiver pairs
    global_auralization_mode (1,:) char = '';                   %String containing auralization mode settings

    automatic_server_start (1,1) logical = true;
    va_ini_path (1,:) char = '';                                %Path to VACore.ini file
end

properties(Access = private)
    va_server_path (1,:) char = '';
end

methods
    function obj = VAScenario( VAServer_path )
        assert(exist('VA', 'class'), 'Cannot find VA class. Did you add the VAMatlab folder to the Matlab path?')
        if nargin > 0
            obj.va_server_path = VAServer_path;
        else
            obj.va_server_path = which('VAServer.exe');
            assert(~isempty(obj.va_server_path), 'VAServer.exe not found in Matlab path. Either add to Matlab path or specify path in constructor.')
        end
        assert(isfile(obj.va_server_path), 'Invalid path to VAServer.exe.')
    end
end

%% Rendering methods
methods (Access = public)
    function realtime_rendering(obj, sources, receiver, tMax)
    %Render a this scenario in real-time using given sources and receiver
        arguments
            obj
            sources (:,:) VASource {mustBeVector}
            receiver (1,1) VAReceiver
            tMax (1,1) {mustBeNumeric, mustBeReal}
        end
        rendering_base(obj, sources, receiver, tMax, false);
    end

    function offline_rendering(obj, sources, receiver, tMax)
    %Render a this scenario in offline using given sources and receiver
        arguments
            obj
            sources (:,:) VASource {mustBeVector}
            receiver (1,1) VAReceiver
            tMax (1,1) {mustBeNumeric, mustBeReal}
        end
        rendering_base(obj, sources, receiver, tMax, true);
    end
end
methods (Access = private)
    % Base rendering function used for offline and real-time rendering
    rendering_base(obj, sources, receiver, tMax, bOffline);
end

end