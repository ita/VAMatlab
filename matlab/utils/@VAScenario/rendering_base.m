function rendering_base(obj, sources, receiver, tMax, bOffline)

arguments
    obj
    sources (:,:) VASource {mustBeVector}
    receiver (1,1) VAReceiver
    tMax (1,1) {mustBeNumeric, mustBeReal}
    bOffline(1,1) logical
end

%% Init
% VA / audio
va = VA( );
if bOffline
    deltaT = obj.block_size / obj.sampling_rate;
else
    deltaT = 1 / obj.motion_update_rate;
end
time_vector = (0:deltaT:tMax)';
nPreSamples = 0;
bHasOutputFile = ~isempty(obj.rendering_output_file);

% Motion trajectories
source_position = zeros( numel(time_vector), 3, numel(sources) );
source_view = zeros( numel(time_vector), 3, numel(sources) );
source_up = zeros( numel(time_vector), 3, numel(sources) );
for idxSource = 1:numel(sources)
    [source_position(:,:,idxSource), source_view(:,:,idxSource), source_up(:,:,idxSource)] = sources(idxSource).trajectory.evaluate(time_vector);
end
[receiver_position, receiver_view, receiver_up] = receiver.trajectory.evaluate(time_vector);

%% Start VA server and establish connection
if obj.automatic_server_start
    va.start_server(obj.va_ini_path, obj.va_server_path);
end
va.connect( 'localhost' );

%% Initialize VA scene

%Auralization mode
if (~isempty(obj.global_auralization_mode))
    va.set_global_auralization_mode(obj.global_auralization_mode)
end

%Renderering modules
init_rendering_modules(obj, va);

source_id = zeros( size(sources) );
signal_source_id = cell( size(sources) );
va.lock_update();
for idxSource = 1:numel(sources)
    %Signal source
    signal_source_id{idxSource} = va.create_signal_source_buffer_from_file( sources(idxSource).signal );
    va.set_signal_source_buffer_playback_action( signal_source_id{idxSource}, 'play' )
    va.set_signal_source_buffer_looping( signal_source_id{idxSource}, true );

    %Sound sources
    source_id(idxSource) = va.create_sound_source( sources(idxSource).name);
    va.set_sound_source_signal_source( source_id(idxSource), signal_source_id{idxSource} );
    va.set_sound_source_sound_power( source_id(idxSource), sources(idxSource).sound_power);
    if ~isempty(sources(idxSource).directivity)
        D = va.create_directivity_from_file( sources(idxSource).directivity );
        va.set_sound_source_directivity( source_id(idxSource), D );
    end
end
va.unlock_update();

%Receiver
receiver_id = va.create_sound_receiver( receiver.name );
if ~isempty(receiver.hrtf)
    H = va.create_directivity_from_file( receiver.hrtf );
    va.set_sound_receiver_directivity( receiver_id, H );
end

%% Timer for synchronization of trajectories (real-time only)
if ~bOffline
    va.set_timer( deltaT );
end

%% Init clock (offline only)
if bOffline
    manual_clock = 0;
    va.set_core_clock( manual_clock );

    % Make sure source and receiver settings are fully transmitted
    pause(0.1);

    % Trigger one block so sources and receiver are actually created
    va.call_module( 'virtualaudiodevice', struct( 'trigger', true ) );
    manual_clock = manual_clock + deltaT;
    va.call_module( 'manualclock', struct( 'time', manual_clock ) );
    nPreSamples = nPreSamples + obj.block_size;
end

%% Init SR-Pair parameters through renderers
init_source_receiver_pair_parameters(obj, va, source_id, receiver_id);

%% Preprocessing so that VDL can fill up
if obj.preprocessing_time > 0
    nPreBlocks = ceil( obj.preprocessing_time * obj.sampling_rate / obj.block_size );
    nPreSamples = nPreSamples + nPreBlocks * obj.block_size;

    for idFrame = 1:nPreBlocks
        if bOffline
            va.call_module( 'virtualaudiodevice', struct( 'trigger', true ) );
            manual_clock = manual_clock + deltaT;
            va.call_module( 'manualclock', struct( 'time', manual_clock ) );
        else
            va.wait_for_timer();
        end
    end
end

%% Run actual auralization

wb = waitbar(0, 'Running Auralization...');
nSamples = numel(time_vector);
for idFrame = 1:nSamples
    
    % Motion update
    va.lock_update();
    for idxSource = 1:numel(sources)
        va.set_sound_source_position( source_id(idxSource), source_position(idFrame, :, idxSource) );
        va.set_sound_source_orientation_view_up( source_id(idxSource), source_view(idFrame, :, idxSource), source_up(idFrame, :, idxSource) );
    end
    va.set_sound_receiver_position( receiver_id, receiver_position(idFrame, :) );
    va.set_sound_receiver_orientation_view_up( receiver_id, receiver_view(idFrame, :), receiver_up(idFrame, :) );
    va.unlock_update();
    
    %TODO:
    %va.set_rendering_module_parameters( obj.renderer_id, struct_with_regular_update );
    
    % Process one block
    if bOffline
        va.call_module( 'virtualaudiodevice', struct( 'trigger', true ) );
        manual_clock = manual_clock + deltaT;
        va.call_module( 'manualclock', struct( 'time', manual_clock ) );
    else
        va.wait_for_timer();
    end

    if isvalid(wb); waitbar(idFrame/nSamples, wb); end
end
if isvalid(wb); close(wb); end
pause(1)

%% Shutdown and disconnect
va.shutdown_server();
pause(3); %Wait for VA to write file
va.disconnect();

%% Remove time used for initializlation/preprocessing
if bHasOutputFile && nPreSamples > 0
    rendering_output_file = cellstr(obj.rendering_output_file);
    nFiles = min(numel(rendering_output_file), numel(cellstr( obj.renderer_id )));

    rendering_output_folder = cellstr(obj.rendering_output_folder);
    if numel(rendering_output_folder) == 1
        rendering_output_folder = repmat(rendering_output_folder, 1, nFiles);
    end

    for idxFile = 1:nFiles
        audio_file = fullfile(rendering_output_folder{idxFile}, rendering_output_file{idxFile});
        audioSamples = audioread(audio_file);
        audioSamples = audioSamples(nPreSamples+1:end, :);
        audiowrite(audio_file, audioSamples, obj.sampling_rate, 'BitsPerSample', 32);
    end
end


%% Rendering module initialization
function init_rendering_modules(obj, va)

% Nothing to init
if isempty(obj.rendering_output_folder) && isempty(obj.rendering_output_file)
    return;
end

if isempty(obj.renderer_id)
    warning('VAScenario.rendering_base(): Trying to set output recording destination for renderer but renderer ID is empty!')
    return;
end

va.lock_update( );

% Create cell strings with matching lengths
renderer_id = cellstr( obj.renderer_id );
nRenderers = numel(renderer_id);
rendering_output_file = cellstr(obj.rendering_output_file);
rendering_output_folder = cellstr(obj.rendering_output_folder);
if numel(rendering_output_folder) == 1
    rendering_output_folder = repmat(rendering_output_folder, 1, nRenderers);
end

%Either no file specified or one per renderer
assert(isempty(obj.rendering_output_file) || numel(rendering_output_file) >= nRenderers, 'There must be one rendering_output_file per specified renderer_id');

% Set output file and init parameters
for idxRenderer = 1:numel(renderer_id)
    if ~isempty(obj.rendering_output_folder)
        va.set_rendering_module_parameters( renderer_id{idxRenderer}, struct('RecordOutputBaseFolder', rendering_output_folder{idxRenderer}) );
    end
    if ~isempty(obj.rendering_output_file)
        va.set_rendering_module_parameters( renderer_id{idxRenderer}, struct('RecordOutputFileName', rendering_output_file{idxRenderer}) );
    end
    va.set_rendering_module_parameters( renderer_id{idxRenderer}, obj.renderering_init_parameters );
end

va.unlock_update( );


function init_source_receiver_pair_parameters(obj, va, source_id, receiver_id)

if isempty(obj.source_receiver_init_parameters)
    return;
end

if isempty(obj.renderer_id)
    warning('VAScenario.rendering_base(): Trying to init source-receiver pair for renderer, but renderer ID is empty!')
    return;
end

% Pause to make sure that source-receiver pair was already created
pause(0.5);

renderer_id = cellstr( obj.renderer_id );
va_struct = obj.source_receiver_init_parameters;
va_struct.sound_receiver_id = receiver_id;
for idxRenderer = 1:numel(renderer_id)
    for idxSource = 1:numel(source_id)
        va_struct.sound_source_id = source_id(idxSource);
        va.set_rendering_module_parameters( renderer_id{idxRenderer}, va_struct );
    end
end
