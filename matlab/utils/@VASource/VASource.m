classdef VASource < handle

    properties
        name (1,:) char = '';                                                   %Name representing the source (optional)
        trajectory (:,1) VATrajectory = VATrajectory.empty;                     %Motion trajectory used during rendering process [VATrajectory]
        directivity (1,:) char = '';                                            %Directivity (daff) file [string]
        signal (1,:) char = '';                                                 %WAVE file used as signal source [string]
        sound_power (1,1) {mustBeNumeric, mustBeReal, mustBePositive} = 1e-4;   %Sound power in watts [scalar]
    end

    methods
        function obj = VASource(trajectory, signal, sound_power, directivity, name)
            arguments
                trajectory (:,1) VATrajectory = VATrajectory.empty
                signal (1,:) char = ''
                sound_power (1,1) {mustBeNumeric, mustBeReal, mustBePositive} = 1e-4
                directivity (1,:) char = ''
                name (1,:) char = ''
            end
            obj.trajectory = trajectory;
            obj.signal = signal;
            obj.sound_power = sound_power;
            obj.directivity = directivity;
            obj.name = name;
        end

        function set.trajectory(obj, value)
            assert(numel(value) <= 1, 'trajectory can only be a scalar or empty')
            obj.trajectory = value;
        end
        function set.directivity(obj, value)
            assert(isempty(value) || startsWith(value, '$(') || endsWith(value, '.daff') , '"directivity" must be of a filename to a .daff file or empty');
            obj.directivity = value;
        end
        function set.signal(obj, value)
            assert(isempty(value) || startsWith(value, '$(') || endsWith(value, '.wav') , '"signal" must be of a filename to a .wav file or empty');
            obj.signal = value;
        end
    end

end