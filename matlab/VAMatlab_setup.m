function VAMatlab_setup( install_permanently )
%VAMatlab_setup Installs VAMatlab by adding the respective directories, as
%well as the directory of the VAServer.exe to the Matlab search path.

%% Directories to be installed
vamatlab_dir = fileparts( mfilename('fullpath') );

vaserver_dir = fullfile( fileparts(vamatlab_dir), 'bin');
vaserver_path = fullfile( vaserver_dir, 'VAServer.exe');
valid_vaserver_path = exist(vaserver_path, 'file');

%% Check if already installed
vamatlab_class_file = which('VA.m', '-all');

already_installed = false;
if numel(vamatlab_class_file) > 1       %Another directory found in path
    already_installed = true;
    vamatlab_class_file = vamatlab_class_file{2};
elseif contains( path(), vamatlab_dir ) %This directory already in path
    already_installed = true;
    vamatlab_class_file = vamatlab_class_file{1};
end
assert(~already_installed, ['[VAMatlab_setup()]: VAMatlab is already installed! Found the following VA class file: "' vamatlab_class_file '"']);

%% No input argument -> Ask user for permission
if nargin < 1
    answer = questdlg('This will add VAMatlab as well as the VAServer.exe to the Matlab search path. Do you want to continue?', ...
        'VAMatlab Setup', 'Yes, add permanently', 'Yes, add for this session', 'Abort', 'Abort');

    switch(lower(answer))
        case 'yes, add permanently'
            install_permanently = true;
        case 'yes, add for this session'
            install_permanently = false;
        otherwise
            return;
    end
end

%% Update Matlab path
%VAMatlab with subdirectories
added_paths = genpath(vamatlab_dir);
addpath( added_paths )

%VAServer
if valid_vaserver_path
    addpath( vaserver_dir );
    added_paths = [added_paths vaserver_dir];
else
    warning('VAMatlab_setup(): Did not find VAServer.exe. Did you change the folder structure of your VA install?');
end

%Add permanently to path?
if install_permanently
    savepath( );
end

%% User notification
if install_permanently
    suffix = '(permanently)';
else
    suffix = '(for this session)';
end
fprintf('[VAMatlab_setup()]: The following directories have been added to the Matlab path %s:\n%s\n', suffix, added_paths);